<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('title');
            $table->string('fulltitle');
            $table->string('image')->nullable();
            $table->string('color_setting')->nullable();
            $table->longText('content')->nullable();
            $table->string('url')->nullable();
            $table->string('parent')->nullable();
            $table->integer('depth')->nullable();
            $table->string('language');
            $table->string('hreflangurl')->nullable();
            $table->string('metatitle')->nullable();
            $table->longText('metadescription')->nullable();
            $table->string('template');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
