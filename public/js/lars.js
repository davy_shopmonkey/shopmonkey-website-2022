$(document).ready(function(){

    $('.contact-form select[name="subject"]').on('change', function(){
        var cur = $(this);
        var form = cur.closest('form');
        cur.closest('.validate-select').removeClass('error');
        if (cur.val() == 'other') {
           form.find('.subject-other').removeClass('d-none').find('input').trigger('focus');
       } else {
           form.find('.subject-other').addClass('d-none').find('input').val('');
       }
    });

    $('.validate-input input').on('focusout', function(){
        var form = $(this).closest('form');
        validateForm(form, $(this).closest('.validate-input'));
    });

    $('.submit-form').on('click', function(){
        $(this).closest('form').submit();
    });

    $('.submit-form').closest('form').on('submit', function(e){
        e.preventDefault();
        var form = $(this);
        var action = form.attr('action');
        var validated = validateForm(form, form.find('.validate-input'));

        if (validated) {

            grecaptcha.ready(function() {
                grecaptcha.execute(recaptchaKey).then(function(token) {
                    $('input[name="g-recaptcha-response"]').val(token);

                    $.ajax({
                        type: "POST",
                        url: action,
                        data: form.serialize(),
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }).done(function (json) {
                        console.log(json);
                        var popup = form.find('.sm-popup');
                        var message = json.message;
                        var status = 'success';

                        // if (form.hasClass('contact-form') && json.status == 'success') {
                        //     gtag("event", "generate_lead");
                        // }

                        if (json.status != 'success') {
                            status = 'error';
                        }

                        popup.removeClass('error success').addClass(status);
                        popup.find('.form-message').html(message);
                        form.find('[data-sm-popup]').trigger('click');

                        if (json.status == 'success') {
                            form.find('input:not([type="hidden"])').val('');
                            form.find('textarea').val('');
                        }

                    });

                });
            });
        }
    });

    $('[data-sm-popup]').on('click', function(e){
        e.preventDefault();
        var cur = $(this);
        var target = cur.attr('href');
        var targetHtml = $(target).clone();

        $('.sm-popup-modal .sm-popup-modal-inner').empty().append(targetHtml);
        $('.sm-popup-overlay, .sm-popup-wrapper, .sm-popup-modal').addClass('active');
        $('html, body').addClass('no-scroll');
    });

    $(document).mouseup(function(e){
        var container = $('.sm-popup-modal');
        if (!container.is(e.target) && container.has(e.target).length === 0 && container.hasClass('active')) {
          closePopup();
        }
    });
    $(document).keydown(function(e) {
        if (e.keyCode == 27) {
          closePopup();
        }
     });

    $('[sm-close-popup]').on('click', function(){
        closePopup();
    });

    $('.faq-wrap .item .question').on('click', function(){
        var cur = $(this);
        var item = cur.closest('.item');
        if (item.hasClass('active')) {
            item.removeClass('active').find('.answer').slideUp();
        } else {
            cur.closest('.faq-wrap').find('.item.active').removeClass('active').find('.answer').slideUp();
            item.addClass('active').find('.answer').slideDown();
        }
    });

    if ($('.references-slider').length > 0) {
        var referenceSlider = new Splide( '.references-slider', {
          arrows: false,
          pagination: false,
          type   : 'loop',
          padding: '30%',
          perPage: 1,
          focus  : 'center',
          autoplay: true,
          pauseOnHover: true,
          keyboard: false,
          autoHeight: true,
          breakpoints: {
              768: {
                  padding: '7%',
              },
              1200: {
                  padding: '15%',
              },
              2000: {
                  padding: '20%',
              }
          }
        }).mount();
    }

    if ($('.featured-projects-slider').length > 0) {
        var featureProjectsSlider = new Splide( '.featured-projects-slider', {
          arrows: false,
          pagination: false,
          //type: 'loop',
          perPage: 3,
          autoplay: false,
          pauseOnHover: true,
          keyboard: false,
          autoHeight: true,
          gap: '3.125em',
          breakpoints: {
              768: {
                  perPage: 1,
                  gap: '1.125em'
              },
              992: {
                  perPage: 2,
                  gap: '2.125em'
              },
              1200: {
                  perPage: 3,
                  gap: '3.125em'
              },
              2000: {
                  perPage: 3,
                  gap: '3.125em'
              }
          }
        }).mount();
    }

    if ($('.projects-slider').length > 0) {
        var projectsSlider = new Splide( '.projects-slider', {
          arrows: false,
          pagination: false,
          type: 'loop',
          autoScroll: {
            speed: 1,
          },
          focus  : 'center',
          perPage: 3,
          autoplay: false,
          pauseOnHover: true,
          keyboard: false,
          autoHeight: true,
          gap: '3.125em',
          breakpoints: {
              768: {
                  perPage: 2,
                  gap: '1.125em'
              },
              992: {
                  perPage: 2,
                  gap: '2.125em'
              },
              1200: {
                  perPage: 3,
                  gap: '3.125em'
              },
              2000: {
                  perPage: 4,
                  gap: '3.125em'
              }
          }
        }).mount( window.splide.Extensions );
    }

    if ($('.reviews-masonry').length > 0) {
        var reviewsMasonry = Macy({
            container: '.reviews-masonry',
            trueOrder: false,
            waitForImages: false,
            margin: 30,
            columns: 3,
            breakAt: {
                1500: {
                    margin: 30,
                    columns: 2
                },
                992: {
                    margin: 30,
                    columns: 1
                }
            }
        });
    }

    if ($('.reviews-slider').length > 0) {
        var reviewsSlider = new Splide( '.reviews-slider', {
            arrows: false,
            pagination: false,
            type: 'loop',
            autoplay: true,
            pauseOnHover: true,
            keyboard: false,
            autoHeight: false,
            perPage: 1,
        }).mount();
    }

    if ($('.brands-slider').length > 0 && $(window).width() < 992) {
        $('.brands-slider').addClass('splide');
        var brandsSlider = new Splide( '.brands-slider', {
            arrows: false,
            pagination: false,
            type: 'loop',
            autoplay: true,
            pauseOnHover: true,
            keyboard: false,
            autoHeight: false,
            perPage: 3,
            destroy: true,
            gap: '4rem',
            focus: 'center',
            breakpoints: {
                499: {
                    // perPage: 2
                },
                992: {
                  destroy: false
              }
          }
        }).mount();
    }

    if ($('.team-slider').length > 0) {
        var teamSlider = new Splide( '.team-slider', {
          drag   : false,
          focus  : 'center',
          autoScroll: {
            speed: 0.5,
          },
          gap: '3.5em',
          arrows: false,
          pagination: false,
          type   : 'loop',
          perPage: 6,
          breakpoints: {
              567: {
                  perPage: 2,
                  gap: '1.5em'
              },
              768: {
                  perPage: 3,
                  gap: '1.5em'
              },
              992: {
                  perPage: 4,
                  gap: '1.5em'
              },
              1200: {
                  perPage: 5,
                  gap: '1.5em'
              },
              2000: {
                  perPage: 6,
                  gap: '3.5em'
              }
          }
        }).mount( window.splide.Extensions );
    }

    if ($('.addons-masonry').length > 0) {
        var reviewsMasonry = Macy({
            container: '.addons-masonry',
            trueOrder: false,
            waitForImages: false,
            margin: 50,
            columns: 3,
            breakAt: {
                1500: {
                    margin: 30,
                    columns: 2
                },
                992: {
                    margin: 30,
                    columns: 1
                }
            }
        });
    }
});


function validateForm(form, target) {
    var validation = true;
    var required = target;

    if (required) {
        $.each(required, function (key, field) {
            var cur = $(this);
            var inputWrap = cur;
            var input = cur.hasClass('validate-select') ? cur.find('select') : cur.find('input');

            if (input.val() == "" || input.val() == " ") {
                inputWrap.removeClass("success").addClass("error");
                validation = false;
            } else {
                inputWrap.removeClass("error").addClass("success");
                input.removeClass("error");
            }

            var name = input.attr("name");
            if (name == "email" && !validateEmail(input.val())) {
                inputWrap.removeClass("success").addClass("error");
                validation = false;
            } else if (name == "email" && validateEmail(input.val())) {
                inputWrap.removeClass("error").addClass("success");
            }

            if (cur.hasClass('validate-select') && !input.val()) {
                inputWrap.removeClass("success").addClass("error");
                validation = false;
            } else if (cur.hasClass('validate-select') && input.val()) {
                inputWrap.removeClass("error").addClass("success");
            }

            inputWrap.addClass("validated");
        });
    }

    return validation;
}

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}


function closePopup() {
    $('.sm-popup-overlay, .sm-popup-wrapper, .sm-popup-modal').removeClass('active');
    $('html, body').removeClass('no-scroll');
    setTimeout(function(){
        $('.sm-popup-modal .sm-popup-modal-inner').empty();
    }, 200);
}
