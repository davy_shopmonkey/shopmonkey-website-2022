$(document).ready(function(){

    $('[data-go-step]').on('click', function(){
        var step = $(this).attr('data-go-step');
        var percentage = (step - 1) * 25;
        var container = $('.shop-steps-wrap');

        container.find('.shop-step-status [data-step]').removeClass('active done');
        for (let i = 1; i <= step; i++) {
          container.find('.shop-step-status [data-step="'+ i +'"]').addClass('done');
        }
        container.find('.shop-step-status [data-step="'+ step +'"]').addClass('active');
        container.find('.bar .inner').css('width', (percentage+'%'));

        container.find('.shop-step').removeClass('active');
        container.find('.shop-step[data-step="'+ step +'"]').addClass('active');
    });

    $('.open-menu, .close-menu').on('click', function(){
        var menu = $('.mobile-menu');
        if (menu.hasClass('active')) {
            menu.removeClass('active');
        } else {
            menu.addClass('active');
        }
    });

});

$(window).scroll(function(){
    var scrollTop = $(window).scrollTop();
    if (scrollTop >= 10) {
        $('#header').addClass('scrolled');
        $('#header .logo .dark').removeClass('d-none');
        $('#header .logo .light').addClass('d-none');
    } else {
        $('#header').removeClass('scrolled');
        $('#header.header-light .logo .dark').addClass('d-none');
        $('#header.header-light .logo .light').removeClass('d-none');
    }
});

// const parallaxContainer = document.getElementById("headline");
// const chouchin = document.querySelectorAll(".icon");
//
// const fixer = 0.003;
//
// if ($('#headline').length > 0) {
//     gsap.registerEffect({
//       name: "mousemoveParallax",
//       effect: (targets, config) => {
//         return gsap.set(targets, { x: config.x, y: config.y });
//       }
//     });
//
//     document.addEventListener("mousemove", function (event) {
//       const pageX =
//         event.pageX - parallaxContainer.getBoundingClientRect().width * 0.5;
//
//       const pageY =
//         event.pageY - parallaxContainer.getBoundingClientRect().height * 0.5;
//
//       chouchin.forEach((item) => {
//         const speedX = item.getAttribute("data-speed-x");
//         const speedY = item.getAttribute("data-speed-y");
//
//         gsap.effects.mousemoveParallax(item, {
//           x: (item.offsetLeft + pageX * speedX) * fixer,
//           y: (item.offsetTop + pageY * speedY) * fixer
//         });
//       });
//     });


// gsap.set(".ball", {xPercent: -50, yPercent: -50});
// gsap.set(".follow-ball", {xPercent: -50, yPercent: -50});
//
// const ball = document.querySelector(".ball");
// const follow = document.querySelector(".follow-ball");
// const pos = { x: window.innerWidth / 2, y: window.innerHeight / 2 };
// const posF = { x: window.innerWidth / 2, y: window.innerHeight / 2 };
// const mouse = { x: pos.x, y: pos.y };
// const speed = 1;
// const speedF = 0.35;
//
// const xSet = gsap.quickSetter(ball, "x", "px");
// const ySet = gsap.quickSetter(ball, "y", "px");
// const xSetFollow = gsap.quickSetter(follow, "x", "px");
// const ySetFollow = gsap.quickSetter(follow, "y", "px");
//
// window.addEventListener("mousemove", e => {
//   mouse.x = e.x;
//   mouse.y = e.y;
// });

// gsap.ticker.add(() => {
//   const dt = 1.0 - Math.pow(1.0 - speed, gsap.ticker.deltaRatio());
//   const dtF = 1.0 - Math.pow(1.0 - speedF, gsap.ticker.deltaRatio());
//   const xSetPos = pos.x += (mouse.x - pos.x) * dt;
//   const ySetPos = pos.y += (mouse.y - pos.y) * dt;
//   const xSetFollowPos = posF.x += (mouse.x - posF.x) * dtF;
//   const ySetFollowPos = posF.y += (mouse.y - posF.y) * dtF;
//
//   xSet(xSetPos);
//   ySet(ySetPos);
//   xSetFollow(xSetFollowPos);
//   ySetFollow(ySetFollowPos);
// });
//
// gsap.registerPlugin(ScrollTrigger);
//
// ScrollTrigger.defaults({
//   // Defaults are used by all ScrollTriggers
//   toggleActions: "restart pause resume pause", // Scoll effect Forward, Leave, Back, Back Leave
//   //markers: true // Easaly remove markers for production
// });
//
// const timelineHeader = gsap.timeline({
//   scrollTrigger: {
//     id: "ZOOM", // Custom label to the marker
//     trigger: "#home", // What element triggers the scroll
//     scrub: 0.5, // Add a small delay of scrolling and animation. `true` is direct
//     start: "top top", // Start at top of Trigger and at the top of the viewport
//     end: "+=100% 50px", // The element is 500px hight and end 50px from the top of the viewport
//     pin: true // Pin the element true or false
//   } });
//
//
// timelineHeader.
// to(".headline .title", {
//   scale: .7 },
// "sameTime");
// }
