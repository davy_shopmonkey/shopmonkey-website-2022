<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\SitemapController;
use App\Http\Controllers\ReferenceController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\NewsletterController;
use App\Http\Controllers\ErrorController;
use App\Http\Controllers\TestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test', [TestController::class, 'index']);

Route::any('api/contact', [ContactController::class, 'index']);
Route::any('api/newsletter', [NewsletterController::class, 'index']);

Route::get('{language}/{type}/{subdirectory}/{subsubdirectory}', [ErrorController::class, 'index'])->middleware('redirect');
Route::get('{language}/{type}/{subdirectory}', [ReferenceController::class, 'index'])->middleware('redirect');

Route::get('sitemap.xml', [SitemapController::class, 'index']);
Route::get('{language}/sitemap.xml', [SitemapController::class, 'language']);
Route::get('{language}/{subdirectory}', [PageController::class, 'index'])->middleware('redirect');
Route::get('{subdirectory}', [PageController::class, 'index'])->middleware('redirect');
Route::get('/', [PageController::class, 'index'])->middleware('redirect');

