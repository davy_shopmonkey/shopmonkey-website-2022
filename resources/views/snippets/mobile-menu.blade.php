<div class="mobile-menu">
    <ul class="title-font">
        @if(isset($links['header']))
        @foreach($links['header'] as $link)
        <li class="{{ Request::url() === smUrl($link->url) ? 'active' : '' }}"><a href="{{ smUrl($link->url) }}" title="{{ $link->title }}">{{ $link->title }}</a></li>
        @endforeach
        @endif
    </ul>
    <div class="close">
        <a class="close-menu">{{ translate('Close') }}</a>
    </div>
</div>
