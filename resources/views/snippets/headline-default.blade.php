<div class="headline-default headline-padding block-dark">
    <div class="container">
        @include('components.title-featured', ['data' => ['title' => $data['title'], 'subtitle' => '', 'content' => $data['content'], 'align' => 'center', 'class' => '', 'type' => 'h1', 'link_class' => $data['link_class'], 'link_text' => $data['link_text'], 'link_url' => $data['link_url'], 'link_icon' => $data['link_icon'], 'class' => 'no-margin']])
    </div>
</div>
