@include('snippets.sm-popup')

<header id="header" class="header-{{ $page->color_setting == 'dark' ? 'dark' : 'light' }}">
<div class="header-fixed-wrap">
    <div class="main-header">
        <div class="container-fluid">
            <div class="inner">
                <div class="header-col menu-col d-xs-flex d-sm-flex d-md-flex d-lg-none d-xl-none">
                    <a href="javascript:;" class="open-menu" title="Open menu">
                        <i class="bx bx-menu-alt-left"></i>
                    </a>
                </div>

                <div class="header-col logo-col">
                    <div class="logo">
                        <a href="{{ smUrl('') }}" title="Logo">
                            <img src="{{ asset('images/logo_color.svg') }}" class="dark {{ $page->color_setting == 'dark' ? '' : 'd-none' }}" alt="Logo" />
                            <img src="{{ asset('images/logo_white.svg') }}" class="light {{ $page->color_setting == 'dark' ? 'd-none' : '' }}" alt="Logo" />
                        </a>
                    </div>
                </div>

                <div class="header-col menu-col d-none d-lg-flex justify-content-center">
                    <div class="main-menu hidden-md hidden-sm hidden-xs body-wrap">
                        <div class="container">
                            <div class="inner">
                                <ul class="main-nav ul-reset">
                                    @if(isset($links['header']))
                                    @foreach($links['header'] as $link)
                                    <li class="{{ Request::url() === smUrl($link->url) ? 'active' : '' }}"><a href="{{ smUrl($link->url) }}" title="{{ $link->title }}">{{ $link->title }}@if($link->url == 'vacatures')<span class="quantity">3</span>@endif</a></li>
                                    @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- <div class="header-col menu-col d-xs-flex d-sm-flex d-md-flex d-lg-none d-xl-none"></div> --}}

                <div class="header-col cta-col">
                    <div class="languages">
                        <ul>
                            @foreach($languages as $language)
                            <li class="{{ session()->get('lang') == $language['language'] ? 'active' : '' }}">
                                <a href="{{ url($language['language'].'/'.$language['url']) }}">
                                <img src="{{ asset('images/flags/'.$language['language'].'.svg') }}" alt="flag-{{ $language['language'] }}">
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <a class="btn bg-tree  d-none d-lg-flex" href="{{ smUrl('contact') }}">{{ translate('Contact') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
</header>

@include('snippets.mobile-menu')
