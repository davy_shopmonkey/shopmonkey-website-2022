{{-- @php
    $faq = [
        'title' => '<span>Veelgestelde</span> vragen',
        'content' => 'Zoveel vragen, zoveel antwoorden. Het komt best wel eens voor dat jouw vraag als eens eerder gesteld is. In dat geval vind je het antwoord hieronder.',
        'questions' => [
            '0' => [
                'title' => 'Ik wil een maatwerk webshop, waar begin ik?',
                'answer' => 'Bij ons natuurlijk!'
            ],
            '1' => [
                'title' => 'Hoe lang is jullie doorlooptijd',
                'answer' => 'Dit is meestal tussen de 2 tot 4 weken. Met Shopmonkey Grow heb je elke week een plekje in de planning!'
            ],
            '2' => [
                'title' => 'Met welke platformen werken jullie?',
                'answer' => 'Wij zijn gefocust op Lightspeed webshops. Heb je wensen die buiten Lightspeed om gaan? Dan kunnen we natuurlijk altijd de mogelijkheden onderzoeken.'
            ],
            '3' => [
                'title' => 'Kan ik partner worden?',
                'answer' => 'Dit kan zeker, neem contact met ons op voor meer informatie.'
            ],
            '4' => [
                'title' => 'Kunnen jullie mijn webwinkel up-to-date houden?',
                'answer' => 'Wij kunnen ervoor zorgen dat het template (of thema) van jouw webshop up-to-date blijft. Het up-to-date houden van content, producten en dergelijken is echter iets wat wij niet doen.'
            ],
        ]
    ]
@endphp --}}


<div class="faq-block block-dark block-padding">
    <div class="container">

        @include('components.title-featured', ['data' => ['title' => translate('Frequently asked questions'), 'subtitle' => '', 'content' => translate('faq-subtitle'), 'align' => 'xl-center', 'class' => '', 'type' => 'div', 'link_class' => 'text-link', 'link_text' => '', 'link_url' => '', 'link_icon' => '', 'class' => '']])

        <div class="faq-wrap">
            @foreach($faq as $item)
            <div class="item">
                <a href="javascript:;" class="question d-flex align-items-center justify-content-between">{{ $item['question'] }}<i class="bx bx-plus-circle"></i></a>
                <div class="answer general-content">{!! $item['answer'] !!}</div>
            </div>
            @endforeach
        </div>

    </div>
</div>
