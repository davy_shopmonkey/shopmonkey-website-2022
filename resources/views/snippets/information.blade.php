<div class="{{ isset($data['boxed']) ? 'container' : '' }}">
    <div class="information-block block-padding block-dark {{ isset($data['boxed']) ? 'boxed' : '' }}">
        <div class="{{ isset($data['boxed']) ? '' : 'container' }}">
            <div class="inner d-flex flex-xl-row flex-column align-items-xl-center">

                @include('components.title-featured', ['data' => ['title' => $data['title'], 'subtitle' => '', 'content' => $data['content'], 'align' => 'left', 'class' => '', 'type' => 'div', 'link_class' => 'text-link', 'link_text' => $data['link_text'], 'link_url' => $data['link_url'], 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'no-margin-desktop']])

                <div class="info-links d-flex flex-lg-row flex-column">
                    <ul class="ul-reset">
                        <li>{{ translate('Design') }}</li>
                        <li>{{ translate('Webshop/website') }}</li>
                        <li>{{ translate('Logo') }}</li>
                        <li>{{ translate('Huisstijl') }}</li>
                    </ul>
                    <ul class="ul-reset">
                        <li>{{ translate('Development') }}</li>
                        <li>{{ translate('Webshop/website development') }}</li>
                        <li>{{ translate('Apps') }}</li>
                        <li>{{ translate('Koppelingen') }}</li>
                        <li>{{ translate('Migraties') }}</li>
                        <li>{{ translate('Online marketing') }}</li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>
