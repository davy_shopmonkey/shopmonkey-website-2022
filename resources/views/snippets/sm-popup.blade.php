<div class="sm-popup-overlay"></div>
<div class="sm-popup-wrapper">
    <div class="sm-popup-modal">
        <a href="javascript:;" sm-close-popup><i class="bx bx-x-circle"></i></a>
        <div class="sm-popup-modal-spacer">
            <div class="sm-popup-modal-inner"></div>
        </div>
    </div>
</div>
