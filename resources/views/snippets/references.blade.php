<div class="references-block block-padding-bottom">
    <div class="references references-slider splide">
        <div class="splide__track">
            <ul class="splide__list">
                @foreach ($references as $reference)
                <li class="splide__slide">@include('components/reference', ['data' => ['class' => 'w-100']])</li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
