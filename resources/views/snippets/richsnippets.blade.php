@if(isset($website))
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@id": "{{ url('#organization') }}",
"@type": "Organization",
"name": "{{ $website->name }}",
"url": "{{ url('') }}",
"foundingDate": "2014",
"sameAs": [
    "{{ $website->facebook }}",
    "{{ $website->instagram }}",
    "{{ $website->linkedin }}"
],
"logo": "{{ asset('images/logo_color.svg') }}",
"address": "{{ $website->address }}, {{ $website->zipcode }}, {{ $website->city }}",
"email": "{{ $website->email }}",
"contactPoint": [
    {
        "@type": "ContactPoint",
        "telephone": "{{ $website->phone }}",
        "contactType": "customer support",
        "availableLanguage" : ["Dutch", "English"]
    }
]
}
</script>

<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@id": "{{ url('#webpage') }}",
    "@type": "WebPage",
    "name": "{{ $website->title }}",
    "url": "{{ Request::url() }}",
    "description": "{{ $website->description }}"
}
</script>

@if($view_name == 'pages.reference')
    <script type="application/ld+json">
    {
        "image": "{{ asset($reference->image) }}",
        "description": "{{ $reference->intro }}",
        "dateModified": "{{ $reference->updated_at }}",
        "datePublished": "{{ $reference->created_at }}",
        "headline": "{{ $reference->title }}",
        "@context": "http://schema.org",
        "@type": "Article"
    }
    </script>
@endif

@endif
