@if($website)
<footer id="footer" class="block-dark block-padding-top">
    <div class="container">

        @include('components.title-featured', ['data' => ['title' => translate('footer-title'), 'subtitle' => translate('footer-subtitle'), 'content' => '', 'align' => 'center', 'class' => '', 'type' => 'div', 'link_class' => '', 'link_text' => '', 'link_url' => '', 'link_icon' => '', 'class' => '']])

        <div class="main-footer d-flex justify-content-between flex-wrap">
            <div class="footer-col">
                <div class="footer-title">Shopmonkey B.V.</div>
                <ul class="footer-spacing ul-reset">
                    <li>{{ $website['address'] }}</li>
                    <li>{{ $website['zipcode'] }} {{ $website['city'] }}</li>
                </ul>

                <ul class="footer-spacing contact ul-reset">
                    <li><a href="mailto:{{ $website['email'] }}" title="{{ $website['email'] }}">{{ $website['email'] }}</a></li>
                    <li><a href="tel:{{ $website['phone'] }}" title="{{ $website['phone'] }}">{{ $website['phone'] }}</a></li>
                </ul>

                <ul class="footer-spacing ul-reset">
                    <li>{{ translate('KvK') }}: {{ $website['coc'] }}</li>
                    <li>{{ translate('BTW') }}: {{ $website['vat'] }}</li>
                    <li>{{ translate('Bank') }}: {{ $website['bank'] }}</li>
                </ul>
            </div>

            <div class="footer-col col-50">
                <div class="footer-title">{{ translate('footer-col-1-title') }}</div>
                <ul class="footer-spacing ul-reset">
                    @if(isset($links['footer_col_1']))
                    @foreach($links['footer_col_1'] as $link)
                    <li><a href="{{ $link->target == '_blank' ? $link->custom_url : smUrl($link->custom_url ? $link->custom_url : $link->url) }}" title="{{ $link->custom_title ? $link->custom_title : $link->title }}" @if($link->target)target="{{ $link->target }}"@endif>{!! $link->custom_title ? $link->custom_title : $link->title !!}</a></li>
                    @endforeach
                    @endif
                </ul>
            </div>

            <div class="footer-col col-50">
                <div class="footer-title">{{ translate('footer-col-2-title') }}</div>
                <ul class="footer-spacing ul-reset">
                    @if(isset($links['footer_col_2']))
                    @foreach($links['footer_col_2'] as $link)
                    <li><a href="{{ $link->target == '_blank' ? $link->custom_url : smUrl($link->custom_url ? $link->custom_url : $link->url) }}" title="{{ $link->custom_title ? $link->custom_title : $link->title }}"  @if($link->target)target="{{ $link->target }}"@endif>{!! $link->custom_title ? $link->custom_title : $link->title !!}</a></li>
                    @endforeach
                    @endif
                </ul>
                {{-- <ul class="footer-spacing ul-reset">
                    <li><a href="" title="">Shopmonkey <strong class="accent-grow">Grow</strong></a></li>
                    <li><a href="" title="">Shopmonkey <strong class="accent-tree">Tree</strong></a></li>
                    <li><a href="" title="">Theme Gibbon</strong></a></li>
                    <li><a href="" title="">Theme Baboon</strong></a></li>
                    <li><a href="" title="">Maatwerk shop</strong></a></li>
                </ul> --}}
            </div>

            <div class="footer-col">
                <div class="footer-title">{{ translate('Subscribe to our newsletter') }}</div>
                <div class="newsletter-wrap">
                    <p class="footer-spacing">{{ translate('newsletter-subtitle') }}</p>
                    <form action="{{ url('api/newsletter') }}" class="footer-spacing d-flex flex-column">
                        <input type="hidden" name="lang" value="{{ session()->get('lang') }}">
                        <input type="hidden" name="g-recaptcha-response">
                        <div class="validate-input">
                            <input type="email" name="email" class="standard-input" placeholder="{{ translate('What is your e-mail address?') }}" required>
                        </div>
                        <a href="javascript:;" class="text-link accent submit-form">{{ translate('Register') }}<i class="bx bx-right-arrow-alt"></i></a>

                        <a href="#form-nl-status" data-sm-popup class="d-none"></a>
                        <div class="sm-popup" id="form-nl-status">
                            <div class="title-wrap text-center">
                                <div class="success">
                                    <i class="bx bxs-check-circle"></i>
                                    <div class="title title-font">{{ translate('Successfully dispatched') }}</div>
                                </div>
                                <div class="error">
                                    <i class="bx bxs-x-circle"></i>
                                    <div class="title title-font">{{ translate('Something went wrong') }}</div>
                                </div>
                            </div>

                            <div class="form-message general-content"></div>
                        </div>

                    </form>
                    <p class="disclaimer">{{ translate('1x per maand, meer niet!') }}</p>
                </div>

                <ul class="footer-spacing contact ul-reset">
                    <li><a href="{{ $website['instagram'] }}" target="_blank" title="{{ translate('Follow us on %1', 'Instagram') }}">{{ translate('Follow us on %1', 'Instagram') }}</a></li>
                    <li><a href="{{ $website['linkedin'] }}" target="_blank" title="{{ translate('Follow us on %1', 'LinkedIn') }}">{{ translate('Follow us on %1', 'LinkedIn') }}</a></li>
                </ul>
            </div>
        </div>

        <div class="footer-logo d-flex flex-column align-items-center text-center">
            <img src="{{ asset('images/index/icon-monkey.svg') }}" alt="Monkey Sticker">
            <div class="slogan title-font">monkey see, monkey do</div>
            <div class="copyright">© {{ date('Y') }} Shopmonkey B.V. All Rights Reserved.</div>
        </div>

    </div>
</footer>
@endif
