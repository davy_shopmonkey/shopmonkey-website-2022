<div class="contact-info-block block-padding">
    <div class="container">

        @include('components.title-featured', ['data' => ['title' => 'Neem contact met ons op', 'subtitle' => '', 'content' => '', 'align' => 'xl-center', 'class' => '', 'type' => 'div', 'link_class' => '', 'link_text' => '', 'link_url' => '', 'link_icon' => '', 'class' => '']])

        <div class="contact-outer-wrap">
            <div class="contact-wrap row">

                <div class="contact-col col-lg-4">
                    <div class="contact-title">Shopmonkey B.V.</div>
                    <ul class="ul-reset">
                        <li>{{ $website['address'] }}</li>
                        <li>{{ $website['zipcode'] }} {{ $website['city'] }}</li>
                    </ul>
                    <ul class="spacer ul-reset">
                        <li>{{ translate('KvK') }}: {{ $website['coc'] }}</li>
                        <li>{{ translate('BTW') }}: {{ $website['vat'] }}</li>
                        <li>{{ translate('Bank') }}: {{ $website['bank'] }}</li>
                    </ul>
                </div>

                <div class="contact-col col-lg-8">
                    <div class="row contact-inner-wrap">
                        <div class="contact-inner-col col-lg-6">
                            <div class="contact-title">{{ translate('Phone') }}: </div>
                            <ul class="ul-reset">
                                <li><a href="tel:+{{ $website['phone'] }}" title="{{ $website['phone'] }}">{{ $website['phone'] }}</a></li>
                            </ul>
                        </div>

                        <div class="contact-inner-col col-lg-6">
                            <div class="contact-title">{{ translate('Email') }}: </div>
                            <ul class="ul-reset">
                                <li><a href="mailto:{{ $website['email'] }}" title="{{ $website['email'] }}">{{ $website['email'] }}</a></li>
                            </ul>
                        </div>

                        <div class="contact-inner-col col-lg-6">
                            <div class="contact-title">{{ translate('Follow us on %1', 'Instagram') }}: </div>
                            <ul class="ul-reset">
                                <li><a href="{{ $website['instagram'] }}" target="_blank" title="{{ translate('Follow us on %1', 'Instagram') }}">{!!  Str::replace('https://www.', '', $website['instagram']) !!}</a></li>
                            </ul>
                        </div>

                        <div class="contact-inner-col col-lg-6">
                            <div class="contact-title">{{ translate('Follow us on %1', 'LinkedIn') }}: </div>
                            <ul class="ul-reset">
                                <li><a href="{{ $website['linkedin'] }}" target="_blank" title="{{ translate('Follow us on %1', 'LinkedIn') }}">{!!  Str::replace('https://www.', '', $website['linkedin']) !!}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
