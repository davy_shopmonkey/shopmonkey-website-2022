<div class="{{ $data['boxed'] ? 'container' : '' }}">
    <div class="cta-banner block-padding block-dark {{ $data['boxed'] ? 'boxed' : '' }}">
        <div class="{{ $data['boxed'] ? '' : 'container' }}">

                @include('components.title-featured', ['data' => ['title' => $data['title'], 'subtitle' => '', 'content' => $data['content'], 'align' => 'left', 'class' => '', 'type' => 'div', 'link_class' => $data['link_class'], 'link_text' => $data['link_text'], 'link_url' => $data['link_url'], 'link_icon' => $data['link_icon'], 'class' => $data['class']]])

        </div>
    </div>
</div>
