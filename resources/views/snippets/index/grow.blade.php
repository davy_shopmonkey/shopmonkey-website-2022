@php
    $reviews = [
        '0' => [
            'image' => 'images/index/grow-review-soclever.jpg',
            'name' => 'Tobias Timmerman',
            'shop' => 'So Clever',
            'score' => '5',
            'date' => '2022-02-01 00:00:00',
            'content' => translate('grow-quote-soclever')
        ]
    ]
@endphp

<div class="grow-block block-padding">
    <div class="container">

        <div class="title-featured-outer d-flex flex-xl-row flex-lg-column align-items-xl-center justify-content-xl-between">
            @include('components.title-featured', ['data' => ['title' => "Let's <span>Grow!</span>", 'subtitle' => 'okayyyy', 'content' => translate('index-grow-intro'), 'align' => 'left', 'class' => '', 'type' => 'div', 'link_class' => 'text-link', 'link_text' => translate('index-grow-cta'), 'link_url' => smUrl('grow'), 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'color-grow']])
            <div class="d-none d-xl-block">
                @foreach ($reviews as $review)
                @include('components.review', ['data' => ['review' => $review, 'compact' => true]])
                @endforeach
            </div>

        </div>

        <div class="block-padding-top">
        @include('components.grow-subscriptions')
        </div>

    </div>
</div>
