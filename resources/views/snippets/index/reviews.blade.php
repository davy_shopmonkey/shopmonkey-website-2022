@php

    $reviews_count = $reviews->count();
    $scores = 0;
    foreach ($reviews as &$review) {
        $scores = $scores + $review['score'];
    }
    $reviews_average = round(($scores / $reviews_count), 1);
@endphp

<div class="featured-reviews block-padding-bottom">
    <div class="container">

        <div class="title-wrap">
            {{-- <div class="score-wrap text-center">
                <span class="count">{{ translate('%1 reviews', $reviews_count) }}</span>
                <span> {{ translate('en') }} </span>
                <span class="score">{{ translate('%1 stars', $reviews_average) }}</span>
            </div> --}}
            @include('components.title-featured', ['data' => ['title' => "Reviews", 'subtitle' => '', 'content' => translate('home-reviews-text', $reviews_average, $reviews_count), 'align' => 'center', 'class' => '', 'type' => 'div', 'link_class' => 'text-link', 'link_text' => translate('index-reviews-cta'), 'link_url' => 'https://www.google.com/search?q=shopmonkey&sxsrf=APq-WBs3lHXbyhD77aqlsZ32rCJXVKh9zA%3A1646077553914&source=hp&ei=cSYdYr7RNMqvkwWj9o-wDQ&iflsig=AHkkrS4AAAAAYh00gcOm595kiKs0o35PRlPki44BfLip&ved=0ahUKEwj-zfvelKP2AhXK16QKHSP7A9YQ4dUDCAk&uact=5&oq=shopmonkey&gs_lcp=Cgdnd3Mtd2l6EAMyBAgjECcyBAgjECcyBAgjECcyCwguEIAEEMcBEK8BMgUIABCABDIFCAAQgAQyBQgAEMsBMgUIABDLATIFCAAQywEyBQgAEMsBOggIABCABBCxAzoRCC4QgAQQsQMQgwEQxwEQ0QM6CwguEIAEEMcBEKMCOg4ILhCABBCxAxDHARDRAzoLCAAQgAQQsQMQgwE6CAguELEDEIMBOgcIABCxAxBDOgsILhCABBCxAxCDAToRCC4QgAQQsQMQgwEQxwEQowI6BggjECcQEzoQCC4QsQMQgwEQxwEQ0QMQQzoFCC4QgAQ6CwguEMcBENEDEMsBUABYkwhg7whoAHAAeACAAbgBiAHBBpIBAzkuMZgBAKABAQ&sclient=gws-wiz#lrd=0x47c5dd50eead325d:0x320405b58e10cbe5,3,,,', 'link_icon' => 'bx bx-right-arrow-alt', 'link_target' => '_blank', 'class' => '']])
        </div>

        <div class="reviews-masonry relative d-none d-lg-block">
            @foreach($reviews as $review)
            @if($loop->index < 15)
            @include('components.review', ['data' => ['review' => $review, 'compact' => false, 'type' => 'desktop']])
            @endif
            @endforeach
        </div>

        <div class="reviews-slider relative d-lg-none splide">
            <div class="splide__track">
                <ul class="splide__list">
                    @foreach($reviews as $review)
                    @if($loop->index < 15)
                    <li class="splide__slide">
                    @include('components.review', ['data' => ['review' => $review, 'compact' => false, 'type' => 'mobile']])
                    </li>
                    @endif
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="text-center">
            <a class="btn btn-large" href="{{ smUrl('reviews') }}">{{ translate('View all reviews') }}</a>
        </div>

    </div>
</div>
