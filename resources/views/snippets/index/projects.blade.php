<div class="featured-projects block-padding">
    <div class="container">
        <div class="title-slider d-flex flex-column flex-xl-row align-items-xl-center justify-content-xl-between">
            @include('components.title-featured', ['data' => ['title' => translate('index-projects-title'), 'subtitle' => '', 'content' => translate('index-projects-content'), 'align' => 'left', 'class' => '', 'type' => 'div', 'link_class' => 'text-link', 'link_text' => translate('Contact us'), 'link_url' => 'contact', 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'no-margin-desktop']])

            <div class="projects featured-projects-slider splide stretch-slider">
                <div class="splide__track">
                    <ul class="splide__list">
                        @foreach ($projects as $project)
                        <li class="splide__slide">@include('components/project')</li>
                        @endforeach
                        <li class="splide__slide"></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
