<div class="brands-block block-padding">
    <div class="container">
        @if($data['partners'])
            @include('components.title-featured', ['data' => ['title' => translate('partners-extra-title'), 'subtitle' => '', 'content' => translate('partners-extra-content'), 'align' => 'center', 'class' => '', 'type' => 'div', 'link_class' => '', 'link_text' => '', 'link_url' => '', 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'mw-title']])
        @else
            @include('components.title-featured', ['data' => ['title' => translate('brands-title'), 'subtitle' => '', 'content' => false, 'align' => 'center', 'class' => '', 'type' => 'div', 'link_class' => '', 'link_text' => '', 'link_url' => '', 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'mw-title']])
        @endif

        <div class="brands-slider">
            <div class="splide__track">
                <ul class="splide__list brands">
                    @if($data['partners'])
                        @foreach($partners as $partner)
                            @if($loop->index > 7)
                            <li class="splide__slide brand-col">@include('components.brand', ['brand' => $partner])</li>
                            @endif
                        @endforeach
                    @else
                        @foreach($brands as $brand)
                            <li class="splide__slide brand-col">@include('components.brand', ['brand' => $brand])</li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
