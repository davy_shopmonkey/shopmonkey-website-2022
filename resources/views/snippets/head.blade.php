<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QN9QJF');</script>
<!-- End Google Tag Manager -->

{{-- <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-1QJRW7NNEK"></script> --}}


<!-- Hotjar Tracking Code for https://www.shopmonkey.nl -->

{{-- <script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:2932560,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script> --}}

{{-- <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-1QJRW7NNEK');
</script> --}}

<meta charset="UTF-8" />
<base href="{{ url('') }}" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="robots" content="{{ App::environment(['local', 'staging']) ? 'noindex, follow' : 'noodp,noydir' }}" />
<meta name="description" content="{{ isset($page->metadescription) ? $page->metadescription : '' }}" />
<meta name="keywords" content="{{ isset($page->metakeywords) ? $page->metakeywords : '' }}" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="google-site-verification" content="c-4qcoN4sDWKLMKdGlX3_gTMScM6i0n-dQbA9j02iEg" />
<meta name="title" content ="{{ isset($page->metatitle) ? $page->metatitle : '' }}" />
<meta name="have-i-been-pwned-verification" value="8c93b65f75d944307c04da4d530fb58f">
<title>{{ isset($page->metatitle) ? $page->metatitle : '' }} - Shopmonkey</title>

<link rel="canonical" href="{{ Request::url() }}"/>

@if(isset($languages))
    @foreach($languages as $language)
    <link rel="alternate" hreflang="{{ $language->language }}" href="{{ url($language->language.'/'.$language->url) }}" />
    @endforeach
@endif

<link rel="shortcut icon" href="{{ asset('images/fav.png') }}" type="image/x-icon" />
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600;700&display=swap" rel="stylesheet">

<link href="{{ asset('css/base.css') }}" rel="stylesheet">
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
<link href="{{ asset('css/lars.css') }}" rel="stylesheet">

<script async id="cookiecode" src="https://cdn.cookiecode.nl/scripts/cookiecode.dist.min.js" data-domain="shopmonkey.nl" data-lang="{{ session()->get('lang') }}"></script>
<script src='https://www.google.com/recaptcha/api.js?render={{ env('GOOGLE_CAPTCHA_KEY') }}'></script>
