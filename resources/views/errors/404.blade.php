@php
    $page = (object)['color_setting' => 'light'];
    $languages = [];
    $page->metatitle = '404 - Not found - Shopmonkey';
    $website = false;
@endphp
@extends('main')

@section('content')

<div id="page-404">
    @include('snippets.headline-default', ['data' => ['title' => '404', 'subtitle' => '', 'content' => translate('404-content'), 'align' => 'center', 'class' => '', 'type' => 'h1', 'link_class' => 'text-link', 'link_text' => translate('404-link-text'), 'link_url' => smUrl(''), 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'no-margin']])
</div>

@endsection
