
<div class="addon">
    <div class="image">
        <div class="image">
            <img src="{{ asset($addon->image) }}">
        </div>
        <div class="info">
            <div class="title title-font">{{ $addon->title }}</div>
            <div class="description">{{ $addon->description }}</div>
        </div>
    </div>
</div>
