<div class="title-featured{{ $data['align'] ? ' text-'.$data['align'].' align-items-'.$data['align'].'' : '' }}{{ $data['class'] ? ' '.$data['class'].'' : '' }}">
    <div class="text-wrap{{ $data['align'] ? ' text-'.$data['align'].' align-items-'.$data['align'].'' : '' }}">
        @if ($data['subtitle'])
            <div class="subtitle">{!! translate($data['subtitle']) !!}</div>
        @endif
        @if ($data['title'])
            <{{ $data['type'] ? $data['type'] : 'div' }} class="title title-font">{!! translate($data['title']) !!}</{{ $data['type'] ? $data['type'] : 'div' }}>
        @endif
        @if ($data['content'])
            <div class="general-content">{!! $data['content'] !!}</div>
        @endif
    </div>

    @if ($data['link_text'] and $data['link_url'])
    <a
        class="featured-link {{ $data['link_class'] }}"
        href="{{ $data['link_url'] }}"
        title="{{ $data['link_text'] }}"
        @if(isset($data['link_target']))
        target="{{ isset($data['link_target']) ? $data['link_target'] : '' }}"
        @endif
    >
        {{ $data['link_text'] }}{!! ($data['link_icon'] ? '<i class="'.$data['link_icon'].'"></i>' : '') !!}
    </a>
    @endif
</div>
