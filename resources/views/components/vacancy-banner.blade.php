<div class="vacancy-banner d-flex flex-column flex-md-row flex-xl-row align-items-xl-center ">
    <div class="image"><img src="{{ asset('images/vacancies/davy.jpg') }}"></div>
    <div class="content">
        <div class="title title-font">
            {{ translate('vacancy-banner-title') }}
        </div>
        <div class="general-content">
            {!! translate('vacancy-banner-text') !!}
        </div>
    </div>
</div>
