@php
    $tags = explode(',', $tags);
@endphp
<ul class="tags d-flex flex-{{ $direction }} flex-wrap ul-reset title-font">
    @foreach ($tags as $tag)
    <li class="{{ strtolower($tag) }}">
        <span>{{ $tag }}</span>
    </li>
    @endforeach
</ul>
