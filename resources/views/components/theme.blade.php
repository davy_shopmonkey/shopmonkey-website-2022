<div class="theme theme-{{ $theme->id }} col-xs-12">
    <div class="inner">
        <div class="content">
            <div class="top">
                <div class="title title-font">{{ $theme->title }}</div>
                <ul class="usp">
                    @for ($i = 1; $i <= 5; $i++)
                    <li><i class="bx bx-check-circle"></i>{{ translate('theme-'. strtolower($theme->title) .'-usp-'.$i) }}</li>
                    @endfor
                    <li><i class="bx bx-check-circle"></i><span>Download in&nbsp;<a href="{{ $theme->themestore_url }}">Lightspeed App Store</a></span></li>
                </ul>
            </div>

            <div class="bottom">
                <div class="price">
                    <div class="money title-font">{{ money($theme->price) }}</div>
                    <div class="per">{{ translate('/month') }}</div>
                </div>
                <div class="links">
                    <a class="btn" href="{{ $theme->example_url }}" target="_blank">{{ translate('View example') }}</a>
                    <a class="small-text-link" href="{{ $theme->documentation_url }}" target="_blank"><i class='bx bx-info-circle'></i>{{ translate('Documentation') }}</a>
                </div>
            </div>

        </div>
        <div class="image d-none d-sm-block">
            <img src="{{ asset($theme->image) }}">
        </div>
    </div>
</div>

<style>
    .theme.theme-{{ $theme->id }} {
        --accent: {{ $theme->accent }};
    }
</style>
