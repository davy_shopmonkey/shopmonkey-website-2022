<div class="reference {{ $data['class'] }}">
    <a href="{{ smUrl($reference['url']) }}" class="inner d-flex flex-column w-100 justify-content-between" title="{{ $reference['shop'] }}">
        <div class="bg bg-cover" style="background-image: url({{ asset($reference['image']) }})"></div>

        @include('components/tags', ['tags' => $reference['tags'], 'direction' => 'row'])

        <div class="content">
            <div class="info">
                <div class="name">{{ $reference['person'] }}</div>
                <div class="shop">{{ $reference['function'] }}</div>
            </div>
            <div class="title title-font">“{{ $reference['quote'] }}”</div>
            <div class="btn btn-small">{{ translate('Read the complete story') }}</div>
        </div>

    </a>
</div>
