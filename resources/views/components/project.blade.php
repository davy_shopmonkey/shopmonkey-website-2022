<div class="project {{ isset($type) ? $type : '' }}">
    <a href="{{ smUrl($project['url']) }}" class="inner d-flex flex-column justify-content-between align-item-start relative block-grey" title="{{ $project['title'] }}">
        <div class="top-info">
            <div class="title title-font" style="color: {{ $project['color'] }};">{{ $project['title'] }}</div>
            <div class="subtitle">{{ $project['description'] }}</div>
            @include('components/tags', ['tags' => $project['tags'], 'direction' => 'column'])
        </div>

        <div class="image {{ $project['image_type'] }}">
            <img src="{{ asset($project['image']) }}" alt="{{ $project['title'] }}">
        </div>

        <div class="text-link">{{ translate('View this project') }}<i class="bx bx-right-arrow-alt"></i></div>
    </a>
</div>
