<div class="review{{ $data['compact'] ? ' compact' : '' }}">
    <div class="review-inner d-flex align-items-start">

        <div class="profile-img d-none d-lg-block">
            <img src="{{ $data['review']['image'] }}" alt="{{ $data['review']['name'] }}">
        </div>

        <div class="review-content">
            <div class="top-info d-flex align-items-center justify-content-between">
                <div class="reviewer d-flex align-items-center">
                    <div class="profile-img d-lg-none">
                        <img src="{{ $data['review']['image'] }}" alt="{{ $data['review']['name'] }}">
                    </div>
                    <div class="profile-wrap">
                        <div class="name">{{ $data['review']['name'] }}</div>
                        @if($data['compact'])
                        <div class="shop">{{ translate('Owner') }} {{ $data['review']['shop'] }}</div>
                        @else
                        <div class="stars">
                            @for ($i = 0; $i < $data['review']['score']; $i++)
                                <i class="bx bxs-star"></i>
                            @endfor
                            @for ($i = 0; $i < 5 - $data['review']['score']; $i++)
                                <i class="bx bxs-star grey"></i>
                            @endfor
                        </div>
                        @endif
                    </div>
                </div>

                <div class="date">{{ strtolower(translate(date("M", strtotime($data['review']['date'])))) }}' {{ date("Y", strtotime($data['review']['date'])) }}</div>
            </div>

            {{-- @if($data['review']['content'])
            <div class="general-content d-lg-none">“{!! Str::limit($data['review']['content'], 100) !!}”</div>
            <div class="general-content d-none d-lg-block">“{!! $data['review']['content'] !!}”</div>
            @unless($data['compact'])
            @if(Str::length($data['review']['content']) > 100)
            <a class="text-link accent text-link-small d-lg-none" href="#review-{{ $data['review']['id'] }}" sm-popup>{{ translate('Lees meer') }}<i class="bx bx-right-arrow-alt"></i></a>
            <div class="review-popup sm-popup" id="review-{{ $data['review']['id'] }}">
                <div class="profile-wrap d-flex align-items-center">
                    <div class="profile-img">
                        <img src="{{ $data['review']['image'] }}" alt="{{ $data['review']['name'] }}">
                    </div>
                    <div class="profile-info">
                        <div class="name">{{ $data['review']['name'] }}</div>
                        @if($data['compact'])
                        <div class="shop">{{ translate('Eigenaar') }} {{ $data['review']['shop'] }}</div>
                        @else
                        <div class="stars">
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="general-content">“{!! $data['review']['content'] !!}”</div>
            </div>
            @endif
            @endunless
            @endif --}}

            @if($data['review']['content'])
            <div class="general-content">“{!! Str::limit($data['review']['content'], 200) !!}”</div>
            @unless($data['compact'])
            @if(Str::length($data['review']['content']) > 200)
            <a class="text-link accent text-link-small" href="#review-{{ $data['type'] }}-{{ $data['review']['id'] }}" data-sm-popup>{{ translate('Lees meer') }}<i class="bx bx-right-arrow-alt"></i></a>
            <div class="review-popup sm-popup" id="review-{{ $data['type'] }}-{{ $data['review']['id'] }}">
                <div class="profile-wrap d-flex align-items-center">
                    <div class="profile-img">
                        <img src="{{ $data['review']['image'] }}" alt="{{ $data['review']['name'] }}">
                    </div>
                    <div class="profile-info">
                        <div class="name">{{ $data['review']['name'] }}</div>
                        @if($data['compact'])
                        <div class="shop">{{ translate('Eigenaar') }} {{ $data['review']['shop'] }}</div>
                        @else
                        <div class="stars">
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                            <i class="bx bxs-star"></i>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="general-content">“{!! $data['review']['content'] !!}”</div>
            </div>
            @endif
            @endunless
            @endif

        </div>

    </div>
</div>
