<form action="{{ url('api/contact') }}" class="contact-form" method="POST" enctype="multipart/form-data">
@csrf
<input type="hidden" name="g-recaptcha-response">
<div class="inputs-wrap flex flex-wrap">

    <div class="input-wrap">
        <div class="validate-input">
            <input class="standard-input" type="text" name="name" placeholder="{{ translate('Name') }}" required>
        </div>
    </div>

    <div class="input-wrap col-50">
        <div class="validate-input">
            <input class="standard-input" type="email" name="email" placeholder="{{ translate('Email') }}" required>
        </div>
    </div>

    <div class="input-wrap col-50">
        <div class="validate-input">
            <input class="standard-input" type="tel" name="phone" placeholder="{{ translate('Telephone number') }}" required>
        </div>
    </div>

    <div class="input-wrap">
        <div class="validate-input">
            <input class="standard-input" type="text" name="subject" value="{{ translate('Vacancy') }}: {{ $vacancy->title }}" required readonly>
        </div>
    </div>

    <div class="input-wrap d-none subject-other">
        <div class="">
            <input class="standard-input" type="text" name="subject_other" placeholder="{{ translate('Please enter your subject here') }}">
        </div>
    </div>

    <div class="input-wrap">
        <textarea class="standard-input" name="message" placeholder="{{ translate('Type your message here') }}..."></textarea>
    </div>

    <input type="file" id="myFile" name="filename">

    <div class="input-wrap">
        <a class="btn btn-large submit-form" href="javascript:;">{{ translate('Contact us') }}</a>
    </div>

</div>

<a href="#form-status" data-sm-popup class="d-none"></a>
<div class="sm-popup" id="form-status">
    <div class="title-wrap text-center">
        <div class="success">
            <i class="bx bxs-check-circle"></i>
            <div class="title title-font">{{ translate('Successfully dispatched') }}</div>
        </div>
        <div class="error">
            <i class="bx bxs-x-circle"></i>
            <div class="title title-font">{{ translate('Something went wrong') }}</div>
        </div>
    </div>

    <div class="form-message general-content"></div>
</div>
</form>


