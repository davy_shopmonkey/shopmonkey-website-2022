<div class="brand">
    <div class="image-wrap">
        <img src="{{ asset($brand['image']) }}" alt="{{ $brand['title'] }}" title="{{ $brand['title'] }}">
    </div>
</div>
