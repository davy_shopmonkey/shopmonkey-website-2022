<div class="portfolio-headline">
    <div class="image-wrap">
        @if($type == 'video')
        <video width="100%" height="100%" preload="auto" muted="" webkit-playsinline="" playsinline="" autoplay="" loop="">
            <source src="{{ asset($src) }}" type="video/mp4">
        </video>
        @else
            <img src="{{ asset($src) }}">
        @endif
    </div>
    <div class="content-wrap overlay-{{ $overlay }}">
        <div class="container">
            <div class="content">
                <h1 class="title">{{ $title }}</h1>
                <div class="subtitle">{{ $subtitle }}</div>
            </div>
        </div>
    </div>
</div>
