<div class="block-padding-top">

    <div class="text-container">

        <div class="d-block d-xl-flex">
            <div class="portfolio-usp">
                <ul>
                    @foreach($usps as $usp)
                    <li><i class="{{ $usp['icon'] }}"></i>{{ $usp['text'] }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="general-content">{!! $intro !!}</div>
        </div>
    </div>

</div>
