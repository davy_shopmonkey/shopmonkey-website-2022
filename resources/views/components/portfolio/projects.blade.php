<div class="portfolio-projects block-padding">
    <div class="container">
        <div class="projects projects-slider splide stretch-slider">
            <div class="splide__track">
                <ul class="splide__list">
                    @foreach ($projects as $project)
                    <li class="splide__slide">@include('components/project')</li>
                    @endforeach
                    {{-- <li class="splide__slide"></li> --}}
                </ul>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide-extension-auto-scroll@0.3.7/dist/js/splide-extension-auto-scroll.min.js"></script>
