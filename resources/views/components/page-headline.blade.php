<div class="page-headline block-padding">
    <div class="container">
        <div class="inner d-flex flex-column flex-xl-row align-items-xl-center justify-content-xl-between">
            <div class="content-col">
                <h1 class="title title-font text-left">
                    {!! $data['title'] !!}
                </h1>
                @if($data['content'])
                <div class="general-content">
                    <p>{!! $data['content'] !!}</p>
                </div>
                @endif
            </div>
            @if($data['image'])
            <div class="image-col">
                <img src="{{ $data['image'] }}" title="{{ $data['title'] }}" alt="{{ $data['title'] }}" class="w-100">
            </div>
            @endif
        </div>
    </div>
</div>
