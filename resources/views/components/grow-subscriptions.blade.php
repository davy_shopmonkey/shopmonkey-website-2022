@php
    $subscriptions = [
        '0' => [
            'title' => 'Grow easy',
            'subtitle' => translate('grow-easy-description'),
            'price' => '85',
            'hours' => '2',
            'type' => 'easy_grow',
            'label' => '',
            'usps' => [
                '0' => [
                    'text' => translate('grow-usp-1'),
                    'active' => true
                ],
                '1' => [
                    'text' => translate('grow-usp-2'),
                    'active' => true
                ],
                '2' => [
                    'text' => translate('grow-usp-3'),
                    'active' => true
                ],
                '3' => [
                    'text' => translate('grow-usp-4'),
                    'active' => true
                ],
                '4' => [
                    'text' => translate('grow-usp-5'),
                    'active' => true
                ],
                '5' => [
                    'text' => translate('grow-usp-6'),
                    'active' => false
                ],
                '6' => [
                    'text' => translate('grow-usp-7'),
                    'active' => false
                ],
                // '8' => [
                //     'text' => translate('grow-usp-8'),
                //     'active' => false
                // ]
            ]
        ],
        '1' => [
            'title' => 'Grow steady 🔥',
            'subtitle' => translate('grow-steady-description'),
            'price' => '79',
            'hours' => '4',
            'type' => 'grow_good',
            'label' => 'Hot!',
            'usps' => [
                '0' => [
                    'text' => translate('grow-usp-1'),
                    'active' => true
                ],
                '1' => [
                    'text' => translate('grow-usp-2'),
                    'active' => true
                ],
                '2' => [
                    'text' => translate('grow-usp-3'),
                    'active' => true
                ],
                '3' => [
                    'text' => translate('grow-usp-4'),
                    'active' => true
                ],
                '4' => [
                    'text' => translate('grow-usp-5'),
                    'active' => true
                ],
                '5' => [
                    'text' => translate('grow-usp-6'),
                    'active' => true
                ],
                '6' => [
                    'text' => translate('grow-usp-7'),
                    'active' => true
                ],
                // '8' => [
                //     'text' => translate('grow-usp-8'),
                //     'active' => false
                // ]
            ]
        ],
        '2' => [
            'title' => 'Grow HUGE',
            'subtitle' => translate('grow-huge-description'),
            'price' => '75',
            'hours' => '8',
            'type' => 'grow_rich',
            'label' => '',
            'usps' => [
                '0' => [
                    'text' => translate('grow-usp-1'),
                    'active' => true
                ],
                '1' => [
                    'text' => translate('grow-usp-2'),
                    'active' => true
                ],
                '2' => [
                    'text' => translate('grow-usp-3'),
                    'active' => true
                ],
                '3' => [
                    'text' => translate('grow-usp-4'),
                    'active' => true
                ],
                '4' => [
                    'text' => translate('grow-usp-5'),
                    'active' => true
                ],
                '5' => [
                    'text' => translate('grow-usp-6'),
                    'active' => true
                ],
                '6' => [
                    'text' => translate('grow-usp-7'),
                    'active' => true
                ],
                // '8' => [
                //     'text' => translate('grow-usp-8'),
                //     'active' => true
                // ]
            ]
        ]
    ]
@endphp

<div class="grow-subscriptions row">
    @foreach($subscriptions as $subscription)
    <div class="item col-xl-4{{ $subscription['label'] ? ' highlight' : '' }}">
        <div class="item-inner">


            <div class="top-info">
                <div class="item-title">
                    <span class="title-font">{{ $subscription['title'] }}</span>
                    @if($subscription['label'])
                    <div class="label d-flex align-items-center justify-content-center">{{ $subscription['label'] }}</div>
                    @endif
                </div>
                <div class="item-subtitle">{!! $subscription['subtitle'] !!}</div>
            </div>

            <ul class="ul-reset usp">
                @foreach($subscription['usps'] as $usp)
                <li class="d-flex align-items-center{{ $usp['active'] ? ' active' : '' }}">
                    <span class="dot"></span>
                    <span class="text">{{ $usp['text'] }}</span>
                </li>
                @endforeach
            </ul>

            <div class="price-wrap d-flex align-items-baseline">
                <div class="price">{{ money($subscription['price']) }}</div>
                <span>{{ translate('p/u') }}</span>
            </div>


            <a href="{{ smUrl('contact?subscription='.$subscription['type'].'') }}" class="btn {{ $subscription['label'] ? ' ' : 'btn-sec' }}">{{ translate('Contact us') }}</a>
            <a class="text-link" href="{{ smUrl('grow') }}">{{ translate('More information') }}</a>
        </div>
    </div>
    @endforeach
</div>
