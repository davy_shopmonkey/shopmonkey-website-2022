<div class="headline-reference relative">
    <img class="img-cover" src="{{ $reference['image'] }}" alt="{{ $reference['person'] }} - {{ $reference['function'] }}">
    <div class="container">
        <div class="inner d-flex align-items-end headline-padding">

            <div class="text-box">
                <div class="details">
                    <div class="name">{{ $reference['person'] }}</div>
                    <div class="shop">{{ $reference['function'] }}</div>
                </div>

                <h1 class="title title-font">{{ $reference['title'] }}</h1>
            </div>

        </div>
    </div>
</div>
