<div class="vacancy {{ $data['class'] }}">
    <a href="{{ smUrl($vacancy['url']) }}" class="inner d-flex flex-column w-100 justify-content-between" title="{{ $vacancy['title'] }}">

        @include('components/tags', ['tags' => $vacancy['tags'], 'direction' => 'row'])

        <div class="title title-font">
            {{ $vacancy['title'] }}
        </div>

        <ul class="usp ul-reset">
            @for ($i = 1; $i <= 3; $i++)
            <li><i class='bx bx-check'></i>{{ translate('vacancy-'. $vacancy['item_id'] .'-usp-'.$i) }}</li>
            @endfor
        </ul>

        <div class="text-link">{{ translate('View vacancy') }}<i class="bx bx-right-arrow-alt"></i></div>

    </a>
</div>
