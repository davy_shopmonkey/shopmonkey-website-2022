@php
$page->color_setting = 'dark';

    $usps = [
        [
            'icon' => 'bx bx-mobile-vibration',
            'text' => translate('portfolio-deryan-usp-1')
        ],
        [
            'icon' => 'bx bx-slider',
            'text' => translate('portfolio-deryan-usp-2')
        ],
        [
            'icon' => 'bx bx-landscape',
            'text' => translate('portfolio-deryan-usp-3')
        ],
        [
            'icon' => 'bx bx-menu',
            'text' => translate('portfolio-deryan-usp-4')
        ],
        [
            'icon' => 'bx bx-cart-alt',
            'text' => translate('portfolio-deryan-usp-5')
        ],
        [
            'icon' => 'bx bx-credit-card',
            'text' => translate('portfolio-deryan-usp-6')
        ]
    ];

@endphp

@extends('main')

@section('content')

        @include('components.portfolio.headline', ['type' => 'image', 'src' => 'images/projects/deryan/headline.jpg', 'title' => translate('portfolio-deryan-headline-title'), 'subtitle' => translate('portfolio-deryan-headline-subtitle'), 'overlay' => 'gradient'])

        @include('components.portfolio.intro', ['usp' => $usps, 'intro' => translate('portfolio-deryan-intro') ])

        @include('components.portfolio.image', ['src' => 'images/projects/deryan/image1.jpg'])

        {{-- @include('components.portfolio.text', ['content' => translate('portfolio-deryan-content-3')]) --}}

        <div class="block-padding-top">
            @include('snippets/cta-banner', ['data' => ['title' => translate('Still not convinced?'), 'content' => translate('cta-block-content'), 'link_class' => 'text-link', 'link_text' => translate('Contact us'), 'link_url' => 'contact', 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'no-margin d-xl-flex flex-xl-row align-items-xl-center justify-content-xl-between link-no-margin-desktop', 'boxed' => false]])
        </div>

        @include('components.portfolio.projects')

@endsection
