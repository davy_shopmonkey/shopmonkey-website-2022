@extends('main')

@section('content')

<div id="about">

    <div class="intro-block">
        <div class="container">
            @include('components.page-headline', ['data' => ['title' => $project->title, 'content' => translate('portolio-temp-content'), 'image' => false]])

        </div>
    </div>

</div>

@endsection
