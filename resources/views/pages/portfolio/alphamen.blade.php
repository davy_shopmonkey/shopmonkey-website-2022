@php
$page->color_setting = 'light';

    $usps = [
        [
            'icon' => 'bx bx-mobile-vibration',
            'text' => translate('portfolio-alphamen-usp-1')
        ],
        [
            'icon' => 'bx bx-credit-card',
            'text' => translate('portfolio-alphamen-usp-2')
        ],
        [
            'icon' => 'bx bx-question-mark',
            'text' => translate('portfolio-alphamen-usp-3')
        ]
    ];

@endphp

@extends('main')

@section('content')

        @include('components.portfolio.headline', ['type' => 'image', 'src' => 'images/projects/alphamen/headline.jpg', 'title' => translate('portfolio-alphamen-headline-title'), 'subtitle' => translate('portfolio-alphamen-headline-subtitle'), 'overlay' => 'gradient'])

        @include('components.portfolio.intro', ['usp' => $usps, 'intro' => translate('portfolio-alphamen-intro') ])

        @include('components.portfolio.image', ['src' => 'images/projects/alphamen/image1.jpg'])

        {{-- @include('components.portfolio.text', ['content' => translate('portfolio-alphamen-content-3')]) --}}

        <div class="block-padding-top">
            @include('snippets/cta-banner', ['data' => ['title' => translate('Still not convinced?'), 'content' => translate('cta-block-content'), 'link_class' => 'text-link', 'link_text' => translate('Contact us'), 'link_url' => 'contact', 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'no-margin d-xl-flex flex-xl-row align-items-xl-center justify-content-xl-between link-no-margin-desktop', 'boxed' => false]])
        </div>

        @include('components.portfolio.projects')

@endsection
