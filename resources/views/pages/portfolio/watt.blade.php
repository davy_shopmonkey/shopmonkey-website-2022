@php
$page->color_setting = 'light';

    $usps = [
        [
            'icon' => 'bx bx-mobile-vibration',
            'text' => translate('portfolio-watt-usp-1')
        ],
        [
            'icon' => 'bx bx-slider',
            'text' => translate('portfolio-watt-usp-2')
        ],
        [
            'icon' => 'bx bx-screenshot',
            'text' => translate('portfolio-watt-usp-3')
        ],
        [
            'icon' => 'bx bxs-user-account',
            'text' => translate('portfolio-watt-usp-4')
        ],
        [
            'icon' => 'bx bx-map',
            'text' => translate('portfolio-watt-usp-5')
        ],
        [
            'icon' => 'bx bx-credit-card',
            'text' => translate('portfolio-watt-usp-6')
        ]
    ];

@endphp

@extends('main')

@section('content')

        @include('components.portfolio.headline', ['type' => 'video', 'src' => 'videos/portfolio/Watt.mp4', 'title' => translate('portfolio-watt-headline-title'), 'subtitle' => translate('portfolio-watt-headline-subtitle'), 'overlay' => 'none'])

        @include('components.portfolio.intro', ['usp' => $usps, 'intro' => translate('portfolio-watt-intro') ])

        @include('components.portfolio.image', ['src' => 'images/projects/watt/image1.jpg'])

        @include('components.portfolio.text', ['content' => translate('portfolio-watt-content-1')])

        @include('components.portfolio.image', ['src' => 'images/projects/watt/image2.jpg'])

        @include('components.portfolio.text', ['content' => translate('portfolio-watt-content-2')])

        @include('components.portfolio.image', ['src' => 'images/projects/watt/image3.jpg'])

        {{-- @include('components.portfolio.text', ['content' => translate('portfolio-watt-content-3')]) --}}

        <div class="block-padding-top">
            @include('snippets/cta-banner', ['data' => ['title' => translate('Still not convinced?'), 'content' => translate('cta-block-content'), 'link_class' => 'text-link', 'link_text' => translate('Contact us'), 'link_url' => 'contact', 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'no-margin d-xl-flex flex-xl-row align-items-xl-center justify-content-xl-between link-no-margin-desktop', 'boxed' => false]])
        </div>

        @include('components.portfolio.projects')

@endsection
