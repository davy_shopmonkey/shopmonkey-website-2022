@php
$page->color_setting = 'light';

    $usps = [
        [
            'icon' => 'bx bx-mobile-vibration',
            'text' => translate('portfolio-detuinmachine-usp-1')
        ],
        [
            'icon' => 'bx bx-cart-alt',
            'text' => translate('portfolio-detuinmachine-usp-2')
        ],
        [
            'icon' => 'bx bx-menu',
            'text' => translate('portfolio-detuinmachine-usp-3')
        ],
        [
            'icon' => 'bx bx-search',
            'text' => translate('portfolio-detuinmachine-usp-4')
        ],
        [
            'icon' => 'bx bx-news',
            'text' => translate('portfolio-detuinmachine-usp-5')
        ],
        [
            'icon' => 'bx bx-code-alt',
            'text' => translate('portfolio-detuinmachine-usp-6')
        ]
    ];

@endphp

@extends('main')

@section('content')

        @include('components.portfolio.headline', ['type' => 'image', 'src' => 'images/projects/detuinmachine/headline.jpg', 'title' => translate('portfolio-detuinmachine-headline-title'), 'subtitle' => translate('portfolio-detuinmachine-headline-subtitle'), 'overlay' => 'gradient'])

        @include('components.portfolio.intro', ['usp' => $usps, 'intro' => translate('portfolio-detuinmachine-intro') ])

        @include('components.portfolio.image', ['src' => 'images/projects/detuinmachine/image1.jpg'])

        {{-- @include('components.portfolio.text', ['content' => translate('portfolio-detuinmachine-content-3')]) --}}

        <div class="block-padding-top">
            @include('snippets/cta-banner', ['data' => ['title' => translate('Still not convinced?'), 'content' => translate('cta-block-content'), 'link_class' => 'text-link', 'link_text' => translate('Contact us'), 'link_url' => 'contact', 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'no-margin d-xl-flex flex-xl-row align-items-xl-center justify-content-xl-between link-no-margin-desktop', 'boxed' => false]])
        </div>

        @include('components.portfolio.projects')

@endsection
