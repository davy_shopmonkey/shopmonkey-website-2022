@php
$page->color_setting = 'light';

    $usps = [
        [
            'icon' => 'bx bx-mobile-vibration',
            'text' => translate('portfolio-eddys-usp-1')
        ],
        [
            'icon' => 'bx bx-stats',
            'text' => translate('portfolio-eddys-usp-2')
        ],
        [
            'icon' => 'bx bx-bell',
            'text' => translate('portfolio-eddys-usp-3')
        ],
        [
            'icon' => 'bx bx-text',
            'text' => translate('portfolio-eddys-usp-4')
        ],
        [
            'icon' => 'bx bx-credit-card',
            'text' => translate('portfolio-eddys-usp-5')
        ],
        [
            'icon' => 'bx bx-line-chart',
            'text' => translate('portfolio-eddys-usp-6')
        ]
    ];

@endphp

@extends('main')

@section('content')

        @include('components.portfolio.headline', ['type' => 'image', 'src' => 'images/projects/eddys/headline.jpg', 'title' => translate('portfolio-eddys-headline-title'), 'subtitle' => translate('portfolio-eddys-headline-subtitle'), 'overlay' => 'gradient'])

        @include('components.portfolio.intro', ['usp' => $usps, 'intro' => translate('portfolio-eddys-intro') ])

        @include('components.portfolio.image', ['src' => 'images/projects/eddys/image1.jpg'])

        {{-- @include('components.portfolio.text', ['content' => translate('portfolio-eddys-content-3')]) --}}

        <div class="block-padding-top">
            @include('snippets/cta-banner', ['data' => ['title' => translate('Still not convinced?'), 'content' => translate('cta-block-content'), 'link_class' => 'text-link', 'link_text' => translate('Contact us'), 'link_url' => 'contact', 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'no-margin d-xl-flex flex-xl-row align-items-xl-center justify-content-xl-between link-no-margin-desktop', 'boxed' => false]])
        </div>

        @include('components.portfolio.projects')

@endsection
