@php
$page->color_setting = 'dark';

    $usps = [
        [
            'icon' => 'bx bx-mobile-vibration',
            'text' => translate('portfolio-woei-usp-1')
        ],
        [
            'icon' => 'bx bx-search',
            'text' => translate('portfolio-woei-usp-2')
        ],
        [
            'icon' => 'bx bxs-dice-6',
            'text' => translate('portfolio-woei-usp-3')
        ],
        [
            'icon' => 'bx bx-news',
            'text' => translate('portfolio-woei-usp-4')
        ],
        [
            'icon' => 'bx bx-credit-card',
            'text' => translate('portfolio-woei-usp-5')
        ]
    ];

@endphp

@extends('main')

@section('content')

        @include('components.portfolio.headline', ['type' => 'image', 'src' => 'images/projects/woei/headline.jpg', 'title' => translate('portfolio-woei-headline-title'), 'subtitle' => translate('portfolio-woei-headline-subtitle'), 'overlay' => 'gradient'])

        @include('components.portfolio.intro', ['usp' => $usps, 'intro' => translate('portfolio-woei-intro') ])

        @include('components.portfolio.image', ['src' => 'images/projects/woei/image1.jpg'])

        {{-- @include('components.portfolio.text', ['content' => translate('portfolio-woei-content-3')]) --}}

        <div class="block-padding-top">
            @include('snippets/cta-banner', ['data' => ['title' => translate('Still not convinced?'), 'content' => translate('cta-block-content'), 'link_class' => 'text-link', 'link_text' => translate('Contact us'), 'link_url' => 'contact', 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'no-margin d-xl-flex flex-xl-row align-items-xl-center justify-content-xl-between link-no-margin-desktop', 'boxed' => false]])
        </div>

        @include('components.portfolio.projects')

@endsection
