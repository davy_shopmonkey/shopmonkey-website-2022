@php
$page->color_setting = 'light';

    $usps = [
        [
            'icon' => 'bx bx-mobile-vibration',
            'text' => translate('portfolio-malelions-usp-1')
        ],
        [
            'icon' => 'bx bx-search',
            'text' => translate('portfolio-malelions-usp-2')
        ],
        [
            'icon' => 'bx bx-list-plus',
            'text' => translate('portfolio-malelions-usp-3')
        ],
        [
            'icon' => 'bx bx-filter',
            'text' => translate('portfolio-malelions-usp-4')
        ],
        [
            'icon' => 'bx bx-question-mark',
            'text' => translate('portfolio-malelions-usp-5')
        ]
    ];

@endphp

@extends('main')

@section('content')

        @include('components.portfolio.headline', ['type' => 'video', 'src' => 'images/projects/malelions/headline.mp4', 'title' => translate('portfolio-malelions-headline-title'), 'subtitle' => translate('portfolio-malelions-headline-subtitle'), 'overlay' => 'gradient'])

        @include('components.portfolio.intro', ['usp' => $usps, 'intro' => translate('portfolio-malelions-intro') ])

        @include('components.portfolio.image', ['src' => 'images/projects/malelions/image1.jpg'])

        {{-- @include('components.portfolio.text', ['content' => translate('portfolio-malelions-content-3')]) --}}

        <div class="block-padding-top">
            @include('snippets/cta-banner', ['data' => ['title' => translate('Still not convinced?'), 'content' => translate('cta-block-content'), 'link_class' => 'text-link', 'link_text' => translate('Contact us'), 'link_url' => 'contact', 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'no-margin d-xl-flex flex-xl-row align-items-xl-center justify-content-xl-between link-no-margin-desktop', 'boxed' => false]])
        </div>

        @include('components.portfolio.projects')

@endsection
