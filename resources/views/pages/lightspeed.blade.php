@extends('main')

@section('content')

<div id="page-webshops">
    @include('components.page-headline', ['data' => ['title' => translate('lightspeed-headline-title'), 'content' => translate('lightspeed-headline-text'), 'image' => asset('images/lightspeed/headline.svg')]])

    <div class="image-text block-padding-top">
        <div class="container">
            <div class="row m-0">
                <div class="image-col col-12 col-xl-6 p-0">
                    <img src="{{ asset('images/lightspeed/dashboard.svg') }}" alt="Design">
                </div>
                <div class="content-col col-12 col-xl-6 p-0 d-flex justify-content-xl-end align-items-center">
                    @include('components.title-featured', ['data' => ['title' => translate('lightspeed-title-1'), 'subtitle' => false, 'content' => translate('lightspeed-content-1'), 'align' => '', 'class' => '', 'type' => 'div', 'link_class' => 'text-link', 'link_text' => translate('lightspeed-link-1-text'), 'link_url' => smUrl('portfolio'), 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'no-margin']])
                </div>
            </div>
        </div>
    </div>

    <div class="image-text reverse block-padding">
        <div class="container">
            <div class="row m-0">
                <div class="image-col col-12 col-xl-6 p-0">
                    <img src="{{ asset('images/lightspeed/workflow.png') }}" alt="Design">
                </div>
                <div class="content-col col-12 col-xl-6 p-0 d-flex align-items-center">
                    @include('components.title-featured', ['data' => ['title' => translate('lightspeed-title-2'), 'subtitle' => false, 'content' => translate('lightspeed-content-2'), 'align' => '', 'class' => '', 'type' => 'div', 'link_class' => 'text-link', 'link_text' => translate('Contact us'), 'link_url' => smUrl('contact'), 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'no-margin']])
                </div>
            </div>
        </div>
    </div>

    <div class="block-padding-bottom">
        @include('snippets.faq', ['faq' => $faqs['webshops']])
    </div>

    <div class="block-padding-bottom">
        @include('snippets.index.projects')
    </div>

</div>

@endsection
