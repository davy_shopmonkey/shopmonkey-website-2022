@extends('main')

@section('content')

<div id="reference">
    @include('components.reference-headline', ['reference' => $reference])

    <div class="block-padding-bottom">

        {{-- <div class="intro-block">
            <div class="container">

                <div class="row intro-row align-items-xl-center">
                    <div class="col-xl-6 intro-col">
                        <div class="intro general-content">
                            {!! $reference['intro'] !!}
                        </div>
                    </div>

                    <div class="col-xl-5 justify-content-xl-end offset-xl-1 intro-col">
                        @include('components.title-featured', ['data' => ['title' => '“'.$reference['quote'].'”', 'subtitle' => '', 'content' => '', 'align' => 'left', 'class' => '', 'type' => 'h2', 'link_class' => '', 'link_text' => '', 'link_url' => '', 'link_icon' => '', 'class' => 'no-margin']])
                    </div>
                </div>

            </div>
        </div> --}}

        <div class="page-content text-container block-margin-top">

            <div class="general-content">
                <blockquote>
                    <p>"{!! $reference['quote'] !!}"</p>
                </blockquote>

                <p>"{!! $reference['intro'] !!}"</p>

            @if($reference['related_project'])
            </div>
            <div class="projects">
                @include('components/project', ['project' => $reference['related_project'], 'type' => 'list'])
            </div>

            <div class="general-content">
            @endif
                {!! $reference['content'] !!}
            </div>
        </div>
    </div>

    @include('snippets/information', ['data' => ['title' => 'Ben je overtuigd? ✨', 'content' => 'We zijn elke dag bezig met het bouwen en verbeteren van de webwinkels van onze klanten. De combinatie van onze <strong>passie</strong> en <strong>ervaring</strong> zorgt er voor dat 99% van onze klanten met een tevreden gevoel terugkijken op onze samenwerking.', 'link_text' => 'Neem contact op', 'link_url' => 'contact']])

    @include('snippets/brands', ['data' => ['partners' => false]])

</div>

@endsection
