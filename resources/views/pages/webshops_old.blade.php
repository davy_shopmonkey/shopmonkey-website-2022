@extends('main')

@section('content')

<div id="page-webshops">
    @include('components.page-headline', ['data' => ['title' => translate('webshops-headline-title'), 'content' => translate('webshops-headline-text'), 'image' => asset('images/webshops/dictionary.svg')]])

    <div class="">
        <div class="container">
            @include('components.title-featured', ['data' => ['title' => translate('webshops-title-1'), 'subtitle' => false, 'content' => translate('webshops-content-1'), 'align' => 'xl-center', 'class' => '', 'type' => 'div', 'link_class' => false, 'link_text' => false, 'link_url' => false, 'link_icon' => false, 'class' => 'no-margin wide-content']])
        </div>
    </div>

    <div class="image-text block-padding-top">
        <div class="container">
            <div class="row m-0">
                <div class="image-col col-12 col-xl-6 p-0">
                    <img src="{{ asset('images/webshops/design-screen.png') }}" alt="Design">
                </div>
                <div class="content-col col-12 col-xl-6 p-0 d-flex justify-content-xl-end align-items-center">
                    @include('components.title-featured', ['data' => ['title' => translate('webshops-title-2'), 'subtitle' => false, 'content' => translate('webshops-content-2'), 'align' => '', 'class' => '', 'type' => 'div', 'link_class' => false, 'link_text' => false, 'link_url' => false, 'link_icon' => false, 'class' => 'no-margin']])
                </div>
            </div>
        </div>
    </div>

    <div class="image-text reverse block-padding">
        <div class="container">
            <div class="row m-0">
                <div class="image-col col-12 col-xl-6 p-0">
                    <img src="{{ asset('images/webshops/code-screen.png') }}" alt="Design">
                </div>
                <div class="content-col col-12 col-xl-6 p-0 d-flex align-items-center">
                    @include('components.title-featured', ['data' => ['title' => translate('webshops-title-3'), 'subtitle' => false, 'content' => translate('webshops-content-3'), 'align' => '', 'class' => '', 'type' => 'div', 'link_class' => false, 'link_text' => false, 'link_url' => false, 'link_icon' => false, 'class' => 'no-margin']])
                </div>
            </div>
        </div>
    </div>

    <div class="block-padding-bottom">
        @include('snippets/information', ['data' => ['title' => translate('information-block-title'), 'content' => translate('information-block-text'), 'link_text' => translate('Contact us'), 'link_url' => smUrl('contact'), 'boxed' => true]])
    </div>

    <div class="block-padding-bottom">
        @include('snippets.faq', ['faq' => $faqs['webshops']])
    </div>

    <div class="block-padding-bottom">
        @include('snippets.index.projects')
    </div>

</div>

@endsection
