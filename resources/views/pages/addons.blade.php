@extends('main')

@section('content')

<div id="page-addons">

    @include('components.page-headline', ['data' => ['title' => translate('addons-headline-title'), 'content' => translate('addons-headline-text'), 'image' => false]])

    <div class="block-padding-bottom">
        <div class="container">
           <div class="addons-masonry addons relative d-block">
               @foreach($addons as $addon)
               @include('components.addon', ['addon' => $addon])
               @endforeach
           </div>
        </div>
    </div>

</div>

@endsection
