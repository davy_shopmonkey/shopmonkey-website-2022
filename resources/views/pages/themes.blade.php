@extends('main')

@section('content')

<div id="page-themes">

    @include('components.page-headline', ['data' => ['title' => translate('themes-headline-title'), 'content' => translate('themes-headline-text'), 'image' => asset('images/themes/headline.svg')]])

    <div class="block-padding-bottom">
        <div class="container">
           <div class="themes row relative d-block">
               @foreach($themes as $theme)
               @include('components.theme', ['theme' => $theme])
               @endforeach
           </div>
        </div>
    </div>

</div>

@endsection
