@php
$icons = [
    '1' => 'bx bx-chat',
    '2' => 'bx bx-analyse',
    '3' => 'bx bxl-figma',
    '4' => 'bx bx-code-alt',
    '5' => 'bx bx-cast'
];
$service_icons = [
    '1' => 'bx bx-customize',
    '2' => 'bx bx-paint',
    '3' => 'bx bx-link',
    '4' => 'bx bxs-dashboard',
    '5' => 'bx bx-add-to-queue',
    '6' => 'bx bxs-arrow-from-left',
];
@endphp

@extends('main')

@section('content')

<div id="page-webshops">
    @include('components.page-headline', ['data' => ['title' => translate('webshops-headline-title'), 'content' => translate('webshops-headline-text'), 'image' => asset('images/webshops/headline.svg')]])

    <div class="block-padding-bottom">
        <div class="container">
        @include('components.title-featured', ['data' => ['title' => translate('webshops-section-1-title'), 'subtitle' => false, 'content' => translate('webshops-section-1-content'), 'align' => 'center', 'class' => '', 'type' => 'div', 'link_class' => 'text-link', 'link_text' => false, 'link_url' => false, 'link_icon' => 'bx bx-right-arrow-alt', 'class' => '']])

        <div class="logo-bubbles">
            <div class="logo-bubble">
                <div class="inner">
                    <img src="{{ asset('images/webshops/lightspeed.svg') }}">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="logo-bubble">
                <div class="inner">
                    <img src="{{ asset('images/webshops/shopify.svg') }}">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="logo-bubble dark">
                <div class="inner">
                    <img src="{{ asset('images/webshops/maatwerk.svg') }}">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>

        <div class="block-padding-top text-center">
            <a class="btn btn-dark" href="{{ smUrl('contact') }}">{{ translate('webshops-section-1-button') }}</a>
        </div>

        </div>
    </div>

    <div class="block-padding block-dark block-margin-bottom block-shop-steps">
        <div class="container">

            <div class="d-flex flex-wrap justify-content-between align-items-start">

                <div>
                @include('components.title-featured', ['data' => ['title' => translate('webshops-section-2-title'), 'subtitle' => false, 'content' => translate('webshops-section-2-content'), 'align' => 'left', 'class' => '', 'type' => 'div', 'link_class' => 'text-link', 'link_text' => false, 'link_url' => false, 'link_icon' => 'bx bx-right-arrow-alt', 'class' => '']])
                <a class="btn" href="{{ smUrl('contact') }}">{{ translate('webshops-section-2-button') }}</a>
                </div>

                <div class="shop-steps-wrap">
                    <div class="shop-step-status">
                        <div class="items d-flex">
                        @for ($i = 1; $i <= 5; $i++)
                            <a class="item {{ $i == 1 ? 'active' : '' }}" data-step="{{ $i }}"  data-go-step="{{ $i }}" href="javascript:;">{{ $i }}. {{ translate('webshop-step-'. $i .'-title-short') }}</a>
                        @endfor
                        </div>
                        <div class="bar"><div class="inner"></div></div>
                    </div>
                    <div class="shop-steps">
                        @for ($i = 1; $i <= 5; $i++)
                            <div class="shop-step {{ $i == 1 ? 'active' : '' }}" data-step="{{ $i }}">
                                <div class="icon"><i class="{{ $icons[$i] }}"></i></div>
                                <div class="title title-font">{{ translate('webshop-step-'. $i .'-title') }}</div>
                                <div class="general-content line-height">{{ translate('webshop-step-'. $i .'-text') }}</div>
                                <div class="buttons">
                                    @if($i < 5)
                                    <a class="btn btn-sec" href="javascript:;" data-go-step="{{ $i + 1 }}">{{ translate('To %1', strtolower(translate('webshop-step-'. ($i+1) .'-title-short'))) }}</a>
                                    @endif
                                    @if($i > 1)
                                    <a class="" href="javascript:;" data-go-step="{{ $i - 1 }}">{{ translate('Back to %1', strtolower(translate('webshop-step-'. ($i-1) .'-title-short'))) }}</a>
                                    @endif
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>

            </div>


        </div>
    </div>

    <div class="block-padding-bottom">
        <div class="portfolio-projects">
            <div class="container">
                <div class="projects projects-slider splide stretch-slider">
                    <div class="splide__track">
                        <ul class="splide__list">
                            @foreach ($projects as $project)
                            <li class="splide__slide">@include('components/project')</li>
                            @endforeach
                            {{-- <li class="splide__slide"></li> --}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="block-padding-bottom">
        <div class="container">
            @include('components.title-featured', ['data' => ['title' => translate('webshops-section-3-title'), 'subtitle' => false, 'content' => translate('webshops-section-3-content'), 'align' => 'center', 'class' => '', 'type' => 'div', 'link_class' => 'text-link', 'link_text' => false, 'link_url' => false, 'link_icon' => 'bx bx-right-arrow-alt', 'class' => '']])
            <div class="services row">
                @for ($i = 1; $i <= 6; $i++)
                <div class="service col-md-4">
                    <div class="inner">
                        <i class="{{ $service_icons[$i] }}"></i>
                        <div class="title title-font">{{ translate('webshop-service-'.$i.'-title') }}</div>
                        <div class="general-content">{{ translate('webshop-service-'.$i.'-description') }}</div>
                    </div>
                </div>
                @endfor
            </div>
        </div>
    </div>

    <div class="">
        <div class="container">
            {{-- @include('components.title-featured', ['data' => ['title' => translate('webshops-section-4-title'), 'subtitle' => false, 'content' => translate('webshops-section-4-content'), 'align' => 'center', 'class' => '', 'type' => 'div', 'link_class' => 'text-link', 'link_text' => false, 'link_url' => false, 'link_icon' => 'bx bx-right-arrow-alt', 'class' => '']]) --}}
        </div>
        @include('snippets/references')
    </div>

    <div class="block-padding-bottom">
        @include('snippets/information', ['data' => ['title' => translate('information-block-title'), 'content' => translate('information-block-text'), 'link_text' => translate('Contact us'), 'link_url' => smUrl('contact'), 'boxed' => true]])
    </div>


    {{-- <div class="block-padding-bottom">
        @include('snippets.faq', ['faq' => $faqs['webshops']])
    </div>

    <div class="block-padding-bottom">
        @include('snippets.index.projects')
    </div> --}}

</div>

<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide-extension-auto-scroll@0.3.7/dist/js/splide-extension-auto-scroll.min.js"></script>


@endsection
