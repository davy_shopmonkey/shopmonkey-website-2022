@extends('main')

@section('content')

<div id="portfolio">

    @include('components.page-headline', ['data' => ['title' => $page['fulltitle'], 'content' => $page['content'], 'image' => false]])

    <div class="portfolio-projects block-padding-bottom">
        <div class="container">
            <div class="projects projects-slider splide stretch-slider">
                <div class="splide__track">
                    <ul class="splide__list">
                        @foreach ($projects as $project)
                        <li class="splide__slide">@include('components/project')</li>
                        @endforeach
                        {{-- <li class="splide__slide"></li> --}}
                    </ul>
                </div>
            </div>
        </div>
    </div>


    @include('snippets/information', ['data' => ['title' => translate('Are you convinced?'), 'content' => translate('information-block-content'), 'link_text' => translate('Contact us'), 'link_url' => smUrl('contact')]])

    @include('snippets/brands', ['data' => ['partners' => false]])

    @include('snippets/cta-banner', ['data' => ['title' => translate('Still not convinced?'), 'content' => translate('cta-block-content'), 'link_class' => 'text-link', 'link_text' => translate('Contact us'), 'link_url' => 'contact', 'link_icon' => 'bx bx-right-arrow-alt', 'class' => 'no-margin d-xl-flex flex-xl-row align-items-xl-center justify-content-xl-between link-no-margin-desktop', 'boxed' => true]])

    <div class="block-padding-top">
    @include('snippets/references')
    </div>

</div>

@endsection

<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide-extension-auto-scroll@0.3.7/dist/js/splide-extension-auto-scroll.min.js"></script>
