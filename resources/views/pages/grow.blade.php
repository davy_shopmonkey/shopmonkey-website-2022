@extends('main')

@section('content')

<div id="page-grow">

    @include('components.page-headline', ['data' => ['title' => translate('grow-headline-title'), 'content' => translate('grow-headline-text'), 'image' => asset('images/grow/headline.svg')]])

    <div class="container">
        <div class="content-box d-flex flex-column flex-xl-row">
            <div class="title title-font">
                {!! translate('grow-explanation-title') !!}
            </div>
            <div class="general-content">
                {!! translate('grow-explanation-content') !!}
            </div>
        </div>
    </div>

    <div class="block-padding">
        <div class="container">
            @include('components.title-featured', ['data' => ['title' => translate('Your benefits'), 'subtitle' => false, 'content' => false, 'align' => 'center', 'class' => '', 'type' => 'div', 'link_class' => false, 'link_text' => false, 'link_url' => false, 'link_icon' => false, 'class' => '']])

            <div class="usp-blocks">
                @for ($i = 1; $i <= 6; $i++)
                <div class="usp-block col-12 col-md-6 col-lg-4">
                    <div class="title title-font">{{ translate('grow-usp-'. $i .'-title') }}</div>
                    <div class="image">
                        <img src="{{ asset('images/grow/usp-'. $i .'.svg') }}" class="w-100">
                    </div>
                </div>
                @endfor
            </div>

        </div>
    </div>

    <div class="block-padding-bottom">
        <div class="container">
            @include('components.title-featured', ['data' => ['title' => translate('ready-to-grow-title'), 'subtitle' => false, 'content' => translate('ready-to-grow-text'), 'align' => 'center', 'class' => '', 'type' => 'div', 'link_class' => false, 'link_text' => false, 'link_url' => false, 'link_icon' => false, 'class' => '']])

            @include('components.grow-subscriptions')


        </div>
    </div>

    <div class="block-padding-bottom">
        @include('snippets/faq', ['faq' => $faqs['grow']])
    </div>

    @include('snippets/references')

</div>

@endsection
