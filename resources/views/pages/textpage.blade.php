@extends('main')

@section('content')

{{-- @include('snippets.headline-default', ['data' => ['title' => $page->title, 'content' => '']]) --}}

<div id="textpage">
    @include('components.page-headline', ['data' => ['title' => $page->title, 'content' => false, 'image' => false]])

    <div class="text-container block-padding-bottom general-content">
        {!! $page->content !!}
    </div>
</div>

@endsection
