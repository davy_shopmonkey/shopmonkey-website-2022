@extends('main')

@section('content')

<div id="page-reviews">

    @include('components.page-headline', ['data' => ['title' => translate('reviews-headline-title'), 'content' => translate('reviews-headline-text'), 'image' => false]])

    <div class="block-padding-bottom">
        <div class="container">
            <div class="reviews-masonry relative d-none d-lg-block">
                @foreach($reviews as $review)
                @include('components.review', ['data' => ['review' => $review, 'compact' => false, 'type' => 'desktop']])
                @endforeach
            </div>

            <div class="reviews-slider relative d-lg-none splide">
                <div class="splide__track">
                    <ul class="splide__list">
                        @foreach($reviews as $review)
                        <li class="splide__slide">
                        @include('components.review', ['data' => ['review' => $review, 'compact' => false, 'type' => 'mobile']])
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
