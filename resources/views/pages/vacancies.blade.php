@extends('main')

@section('content')

<div id="page-vacancies">

    @include('components.page-headline', ['data' => ['title' => translate('vacancy-headline-title'), 'content' => translate('vacancy-headline-text'), 'image' => asset('images/vacancies/headline.jpg')]])

    <div class="block-padding-bottom">
        <div class="container">
            <div class="vacancies row">
            @foreach ($vacancies as $vacancy)
            @include('components/vacancy', ['data' => ['class' => 'col-12 col-lg-6']])
            @endforeach
            </div>
        </div>
    </div>

    <div class="block-padding-bottom">
        <div class="container">
            @include('components/vacancy-banner')
        </div>
    </div>

</div>

@endsection
