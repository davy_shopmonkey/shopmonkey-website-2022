@extends('main')

@section('content')

<div id="about">

    <div class="intro-block">
        <div class="container">
            @include('components.page-headline', ['data' => ['title' => translate('about-headline-title'), 'content' => translate('about-headline-text'), 'image' => false]])

        </div>
    </div>

    <div class="team-slider splide">
      <div class="splide__track">
            <ul class="splide__list">
                @for ($i = 1; $i <= 12; $i++)
                <li class="splide__slide">
                    <img src="{{ asset('images/team/Expert'. $i .'.jpg') }}" alt="Shopmonkey Expert" title="Shopmonkey Expert" width="350" height="500">
                </li>
                @endfor
            </ul>
      </div>
    </div>

    <div class="image-text block-margin-top">
        <div class="row m-0">
            <div class="col-xl-6 d-flex justify-content-end align-items-center block-grey image-wrap">
                    <img src="{{ asset('images/about/about-monkey-shop.svg') }}" alt="MonkeyShop">
            </div>
            <div class="col-xl-6 d-flex align-items-center content-wrap">
                    @include('components.title-featured', ['data' => ['title' => translate('about-block-1-title'), 'subtitle' => '', 'content' => translate('about-block-1-text'), 'align' => 'left', 'class' => '', 'type' => 'div', 'link_class' => '', 'link_text' => '', 'link_url' => '', 'link_icon' => '', 'class' => '']])
            </div>
        </div>
    </div>

    <div class="image-text-center block-padding">
        <div class="container">
            @include('components.title-featured', ['data' => ['title' => translate('about-block-2-title'), 'subtitle' => '', 'content' => translate('about-block-2-text'), 'align' => 'xl-center', 'class' => '', 'type' => 'div', 'link_class' => '', 'link_text' => '', 'link_url' => '', 'link_icon' => '', 'class' => '']])
            <div class="image-wrap">
                <img src="{{ asset('images/about/about-codescreen.png') }}" alt="" width="1400" height="770">
            </div>
        </div>
    </div>

    <div class="image-text reverse block-margin-top">
        <div class="row m-0">
            <div class="col-xl-6 d-flex justify-content-start align-items-center block-grey image-wrap">
                    <img src="{{ asset('images/about/about-monkey-shop.svg') }}" alt="MonkeyShop">
            </div>
            <div class="col-xl-6 d-flex align-items-center content-wrap">
                    @include('components.title-featured', ['data' => ['title' => translate('about-block-3-title'), 'subtitle' => '', 'content' => translate('about-block-3-text'), 'align' => 'left', 'class' => '', 'type' => 'div', 'link_class' => '', 'link_text' => '', 'link_url' => '', 'link_icon' => '', 'class' => '']])
            </div>
        </div>
    </div>

    <div class="outro-block block-padding">
        <div class="container">

            <div class="emblem text-center">
                <img src="{{ asset('images/logo-emblem.svg') }}" alt="Monkey">
            </div>

            @include('components.title-featured', ['data' => ['title' => translate('about-block-4-title'), 'subtitle' => '', 'content' => translate('about-block-4-text'), 'align' => 'xl-center', 'class' => 'no-margin', 'type' => 'div', 'link_class' => '', 'link_text' => '', 'link_url' => '', 'link_icon' => '']])

        </div>
    </div>



{{--     <img src="{{ asset('images/about/about-codescreen.png') }}" alt="Code screen"> --}}


</div>

@endsection

<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide-extension-auto-scroll@0.3.7/dist/js/splide-extension-auto-scroll.min.js"></script>
