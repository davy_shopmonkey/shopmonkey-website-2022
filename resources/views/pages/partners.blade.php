@extends('main')

@section('content')

<div id="page-partners">

    @include('components.page-headline', ['data' => ['title' => translate('partners-headline-title'), 'content' => translate('partners-headline-text'), 'image' => false]])

    <div class="partners-block">
        <div class="container">
            <div class="partners d-flex flex-column d-lg-block">
                <div class="center-item d-flex justify-content-center align-items-center">
                    <div class="sm-logo">
                        <img src="{{ asset('images/logo-emblem.svg') }}" alt="Shopmonkey">
                    </div>
                    <div class="link">
                        <img src="{{ asset('images/partners/link-icon.svg') }}" alt="Link">
                    </div>
                    <div class="title title-font">
                        {!! translate('Jouw<br><span>winkel.</span>') !!}
                    </div>
                </div>
                @foreach($partners as $partner)
                    @if($loop->index < 8)
                        <div class="partner partner-{{ $partner['order'] }}">
                            <img src="{{ asset($partner['image']) }}" alt="{{ $partner['title'] }}">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    @endif
                @endforeach
                {{-- <div class="partner partner-1">
                    <img src="{{ asset('images/partners/lightspeed.svg') }}" alt="">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div class="partner partner-2">
                    <img src="{{ asset('images/partners/elastic-web.svg') }}" alt="">
                    <span></span>
                    <span></span>
                    <span></span>
                 </div>
                <div class="partner partner-3">
                    <img src="{{ asset('images/partners/trusted-shops.svg') }}" alt="">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div class="partner partner-4">
                    <img src="{{ asset('images/partners/online-id.svg') }}" alt="">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div class="partner partner-5">
                    <img src="{{ asset('images/partners/mollie.svg') }}" alt="">
                    <span></span>
                    <span></span>
                    <span></span>
                 </div>
                <div class="partner partner-6">
                    <img src="{{ asset('images/partners/droppery.svg') }}" alt="">
                    <span></span>
                    <span></span>
                    <span></span>
                 </div>
                <div class="partner partner-7">
                    <img src="{{ asset('images/partners/online-distributeur.svg') }}" alt="">
                    <span></span>
                    <span></span>
                    <span></span>
                 </div>
                <div class="partner partner-8">
                    <img src="{{ asset('images/partners/shopify.svg') }}" alt="">
                    <span></span>
                    <span></span>
                    <span></span>
                </div> --}}
            </div>
        </div>
    </div>

    {{-- <div class="block-padding-top">
        @include('snippets.faq', ['faq' => $faqs['partners']])
    </div> --}}

    @include('snippets/brands', ['data' => ['partners' => true]])

</div>

@endsection
