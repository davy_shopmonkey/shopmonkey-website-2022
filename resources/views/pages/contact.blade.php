@php
    $reviews = [
        '0' => [
            'id' => '1456789',
            'image' => 'images/reviews/wattfietsen.jpg',
            'name' => 'Jimmy Meerbach',
            'shop' => 'WATT fietsen',
            'score' => '5',
            'date' => '2021-10-04 00:00:00',
            'content' => 'Zeer goed geholpen door Shopmonkey. Kunnen snel schakelen en leveren binnen korte tijd een zeer professionele webshop, tegen een scherpe prijs!'
        ]
    ]
@endphp

@extends('main')

@section('content')

<div id="contact">

    <div class="form-block block-padding headline-padding">
        <div class="container">

            <div class="row align-items-center">
                <div class="col-xl-5">
                    @include('components.title-featured', ['data' => ['title' => translate('contact-title'), 'subtitle' => '', 'content' => translate('contact-text'), 'align' => 'left', 'class' => '', 'type' => 'h1', 'link_class' => '', 'link_text' => '', 'link_url' => '', 'link_icon' => '', 'class' => '']])
                    <div class="d-none d-xl-block">
                        @include('components.review', ['data' => ['review' => $highlight_review, 'compact' => false, 'type' => 'desktop']])
                    </div>
                </div>
                <div class="col-xl-6 offset-xl-1">
                    @include('components.contact-form')
                </div>
            </div>

        </div>
    </div>

    @include('snippets/faq', ['faq' => $faqs['contact']])

    @include('snippets/contact-info')

</div>

@endsection
