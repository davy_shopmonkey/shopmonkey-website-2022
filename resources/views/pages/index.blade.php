@extends('main')

@section('content')

<div id="headline" class="headline">
    <div class="container">
        <div class="inner d-flex align-items-center justify-content-between">
            <div class="content-col">
                <h1 class="title title-font text-left">
                    {!! translate('headline-title') !!}
                </h1>

                <div class="general-content">
                    <p>{!! translate('headline-text-1') !!}</p>
                    <p>{!! translate('headline-text-2', '<a href="'. translate('headline-url') .'">'. translate('het realiseren van webshops') .'</a>') !!}</p>
                </div>

                {{-- <div class="icon icon-1 d-none d-md-block" data-speed-x="2" data-speed-y="1"><span><img src="{{ asset('images/index/icon-tree.svg') }}"></span></div>
                <div class="icon icon-2 d-none d-md-block" data-speed-x="7" data-speed-y="5"><span><img src="{{ asset('images/index/icon-grow.svg') }}"></span></div>
                <div class="icon icon-3 d-none d-md-block" data-speed-x="2" data-speed-y="1"><span><img src="{{ asset('images/index/icon-checkout.svg') }}"></span></div>
                <div class="icon icon-4 d-none d-md-block" data-speed-x="7" data-speed-y="5"><span><img src="{{ asset('images/index/icon-monkey.svg') }}"></span></div> --}}
                <a class="btn btn-large" href="{{ smUrl('contact') }}">{{ translate('Contact us') }}</a>
            </div>
            <div class="d-none d-sm-block image-col">
                <img src="{{ asset('images/index/headline.svg') }}" title="{!! translate('headline-title') !!}" alt="{!! translate('headline-title') !!}" class="w-100">
            </div>
            {{-- <div class="cta-block d-flex align-items-center justify-content-between flex-column flex-md-row flex-lg-row">
                <div class="content">{{ translate('headline-cta') }}📱</div>
                <a class="btn btn-large" href="{{ url('contact') }}">{{ translate('Contact us') }}</a>
            </div> --}}
        </div>
    </div>
</div>

@include('snippets/index/grow')
@include('snippets/references')
@include('snippets/information', ['data' => ['title' => translate('information-block-title'), 'content' => translate('information-block-text'), 'link_text' => translate('Contact us'), 'link_url' => smUrl('contact')]])
@include('snippets/index/projects')
@include('snippets/index/reviews')

@endsection
