@extends('main')

@section('content')

<div id="page-references">

    @include('components.page-headline', ['data' => ['title' => translate('references-headline-title'), 'content' => translate('references-headline-text'), 'image' => false]])

    <div class="block-padding-bottom">
        <div class="container">
            <div class="references row">
            @foreach ($references as $reference)
            @include('components/reference', ['data' => ['class' => 'col-12 col-lg-6']])
            @endforeach
            </div>
        </div>
    </div>

</div>

@endsection
