@extends('main')

@section('content')

<div id="vacancy">

    <div class="container">
        <div class="vacancy-headline block-padding">
            <h1 class="title">{{ $vacancy->title }}</h1>
            <div class="tags-wrap">
            @include('components/tags', ['tags' => $vacancy['tags'], 'direction' => 'row'])
            </div>
            <ul class="usp ul-reset">
                @for ($i = 1; $i <= 3; $i++)
                <li><i class='bx bx-check'></i>{{ translate('vacancy-'. $vacancy['item_id'] .'-usp-'.$i) }}</li>
                @endfor
            </ul>
            <a class="btn" href="mailto:vacatures@shopmonkey.nl">{{ translate('Apply now') }}</a>
        </div>
    </div>

    <div class="vacancy-content-wrap block-padding-bottom">
        <div class="container">
            <div class="content">
            <div class="general-content">
                {{-- <p><strong>{!! $vacancy->description !!}</strong></p> --}}
                {!! $vacancy->content !!}
            </div>
            </div>
        </div>
    </div>

    {{-- <div class="block-padding-bottom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-5">
                    @include('components.title-featured', ['data' => ['title' => translate('vacancy-contact-title'), 'subtitle' => '', 'content' => translate('vacancy-contact-text'), 'align' => 'left', 'class' => '', 'type' => 'h1', 'link_class' => '', 'link_text' => '', 'link_url' => '', 'link_icon' => '', 'class' => '']])
                </div>
                <div class="col-xl-6 offset-xl-1">
                    @include('components/vacancy-contact')
                </div>
            </div>
        </div>
    </div> --}}

    <div class="block-padding-bottom">
        <div class="container">
            @include('components/vacancy-banner')
        </div>
    </div>



</div>

@endsection
