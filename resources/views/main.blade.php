<!DOCTYPE html>
<html lang="{{ session()->get('lang') }}">
    <head>
        @include('snippets.head')
    </head>

    <body class="{{ isset($page->title) ? str_replace(' ', '-', strtolower($page['title'])) : '' }}">

        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QN9QJF"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <div class="body-wrap">

            {{-- <div class="d-none d-lg-block">
                <div class="ball"></div>
                <div class="follow-ball"></div>
            </div> --}}
            @if($website)
            @include('snippets.richsnippets')
            @endif
            @include('snippets.header')

            <main>
                @yield('content')
            </main>

            @include('snippets.footer')

            @include('snippets.scripts')
        </div>
    </body>
</html>
