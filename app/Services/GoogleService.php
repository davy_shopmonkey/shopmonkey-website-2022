<?php


namespace App\Services;
use League\OAuth2\Client\Provider\Google;

class GoogleService
{
    //private $shop;

    public function __construct()
    {
        //$this->shop = Shop::where('id', auth()->user()->current_shop_id)->first();
    }

    public function getReviews()
    {

        $accessToken = $this->getAccessToken();

        $pageToken = 1;
        $all_reviews = [];

        while($pageToken) {
          $curl = curl_init();

          curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://mybusiness.googleapis.com/v4/accounts/'. env('GOOGLE_ACCOUNT_ID') .'/locations/'. env('GOOGLE_LOCATION_ID') .'/reviews?pageSize=50'. (($pageToken != 1) ? ('&pageToken='.$pageToken) : ''),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
              'Authorization: Bearer '. $accessToken
            ),
          ));

          $response = curl_exec($curl);
          $response = json_decode($response, true);

          $pageToken = isset($response['nextPageToken']) ? $response['nextPageToken'] : false;

          $averageRating = $response['averageRating'];
          $totalReviewCount = $response['totalReviewCount'];

          if (isset($response['reviews'])) {
              foreach($response['reviews'] as $review) {
                  $all_reviews[] = $review;
              }
          }
          curl_close($curl);

        }

        $data = [
            'reviews' => $all_reviews,
            'averageRating' => $averageRating,
            'totalReviewCount' => $totalReviewCount
        ];

        return $data;

    }

    private function getAccessToken()
    {

        session_start(); // Remove if session.auto_start=1 in php.ini

        $refreshToken = env('GOOGLE_REFRESH_TOKEN') ? env('GOOGLE_REFRESH_TOKEN') : false;
        //$refreshToken = false;

        $provider = new Google([
            'clientId'     => env('GOOGLE_CLIENT_ID'),
            'clientSecret' => env('GOOGLE_CLIENT_SECRET'),
            'redirectUri'  => 'http://127.0.0.1:8000/test',
            'accessType'   => 'offline',
            //'hostedDomain' => 'example.com', // optional; used to restrict access to users on your G Suite/Google Apps for Business accounts
        ]);

        if (!$refreshToken) {
            if (!empty($_GET['error'])) {

                // Got an error, probably user denied access
                exit('Got error: ' . htmlspecialchars($_GET['error'], ENT_QUOTES, 'UTF-8'));

            } elseif (empty($_GET['code']) && !$refreshToken) {

                // If we don't have an authorization code then get one
                $authUrl = $provider->getAuthorizationUrl([
                    'scope' => [
                        'https://www.googleapis.com/auth/business.manage'
                    ]
                ]);
                $_SESSION['oauth2state'] = $provider->getState();
                header('Location: ' . $authUrl);
                exit;

            } elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {

                // State is invalid, possible CSRF attack in progress
                unset($_SESSION['oauth2state']);
                exit('Invalid state');

            } else {

                // Try to get an access token (using the authorization code grant)
                $token = $provider->getAccessToken('authorization_code', [
                    'code' => $_GET['code'],
                ]);

                echo 'Refresh token. Sla deze op in env:<br>'. $token->getRefreshToken();
                exit;
                //return $token->getToken();

                // Use this to interact with an API on the users behalf
                //echo $token->getToken();

                // Use this to get a new access token if the old one expires


            }
        } else {

            $token = $provider->getAccessToken('refresh_token', [
                'refresh_token' => $refreshToken,
            ]);

            return $token->getToken();

        }

    }

    public function verifyCaptcha($response)
    {

        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = array(
            'secret' => env('GOOGLE_CAPTCHA_SECRET'),
            'response' => $response
        );
        $options = array(
            'http' => array (
                'header' => 'Content-Type: application/x-www-form-urlencoded',
                'method' => 'POST',
                'content' => http_build_query($data)
            )
        );

        $context  = stream_context_create($options);
        $verify = file_get_contents($url, false, $context);
        $captcha_success=json_decode($verify);

        if ($captcha_success->success==true) {
            return true;
        } else {
            return false;
        }

    }

}
