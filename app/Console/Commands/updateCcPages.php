<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Page;

class updateCcPages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update-cc-pages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Privacy Policy and Cookies pages.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $pages = [
            [
                'language' => 'nl',
                'url' => 'privacybeleid',
                'endpoint' => 'https://cdn.cookiecode.nl/privacy/shopmonkey.nl/nl/json'
            ],
            [
                'language' => 'en',
                'url' => 'privacy-policy',
                'endpoint' => 'https://cdn.cookiecode.nl/privacy/shopmonkey.nl/en/json'
            ],
            [
                'language' => 'nl',
                'url' => 'cookies',
                'endpoint' => 'https://cdn.cookiecode.nl/cookie/shopmonkey.nl/nl/json'
            ],
            [
                'language' => 'en',
                'url' => 'cookies',
                'endpoint' => 'https://cdn.cookiecode.nl/cookie/shopmonkey.nl/en/json'
            ],
        ];

        foreach($pages as $page) {

            $json = file_get_contents($page['endpoint']);
            $obj = json_decode($json);
            $html = '';

            foreach($obj->Blocks as $index => $block) {
                $html .= '<h2>'. ($index + 1) .' '. $block->Title .'</h2>';
                $html .= $block->DescriptionShort;
            }

            $page = Page::where(['url' => $page['url'], 'language' => $page['language']])->first()->update(['content' => $html]);


        }

    }
}
