<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\GoogleService;
use App\Models\Review;

class UpdateGoogleReviews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update-google-reviews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get reviews from Google and put them in database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $googleService = new GoogleService;  // correct
        $reviews = $googleService->getReviews();
        $reviews = $reviews['reviews'];

        $ratings = [
            'FIVE' => 5,
            'FOUR' => 4,
            'THREE' => 3,
            'TWO' => 2,
            'ONE' => 1
        ];

        Review::truncate();

        foreach($reviews as $review) {

                $languages = isset($review['comment']) ? explode('(Original)', $review['comment']) : [];

                $review_per_lang = [
                    'nl' => isset($languages[1]) ? $languages[1] : '',
                    'en' => isset($languages[0]) ? $languages[0] : '',
                ];

                foreach($review_per_lang as $index => $language) {
                    $newUser = Review::create([
                        'review_id' => $review['reviewId'],
                        'image' => $review['reviewer']['profilePhotoUrl'],
                        'name' => $review['reviewer']['displayName'],
                        'score' => $ratings[$review['starRating']],
                        'content' => trim($language),
                        'date' => $review['createTime'],
                        'language' => $index
                    ]);
                }

        }

    }
}
