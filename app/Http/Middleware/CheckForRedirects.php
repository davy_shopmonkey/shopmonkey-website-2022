<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Redirect;

class CheckForRedirects
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $route = $request->path();
        $route = trim($route, '/');
        $route = '/'.$route;
        $redirect = Redirect::where('old', $route)->first();

        if ($redirect) {
            return redirect($redirect['new'], $redirect['type']);
        }

        return $next($request);

    }
}
