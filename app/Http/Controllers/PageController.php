<?php

namespace App\Http\Controllers;
use App\Models\Page;
use App\Models\Language;
use App\Models\Navigation;
use App\Models\Website;
use App\Models\Reference;
use App\Models\Project;
use App\Models\Tag;
use App\Models\Brand;
use App\Models\Review;
use App\Models\Faq;
use App\Models\Partner;
use App\Models\Addon;
use App\Models\Theme;
use App\Models\Vacancy;

use Illuminate\Http\Request;

class PageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($language, $subdirectory = '')
    {
        // talen check als middleware aanmaken.
        $subdirectory = $subdirectory ? $subdirectory : null;
        $page = Page::where(['language' => $language, 'url' => $subdirectory])->first();

        if (!$this->isActiveLanguage($language)) {

            $subdirectory = $language;
            return redirect($this->currentLanguage(). '/' . $subdirectory .'/');

        } else {

            $links = $this->getLinks($language);
            $faqs = $this->getFaqs($language);

            $website = Website::first();
            $brands = Brand::inRandomOrder()->limit(10)->orderBy('order', 'ASC')->get();
            $partners = Partner::orderBy('order', 'ASC')->get();
            $references = Reference::get(['references.language' => $language]);
            $projects = Project::get(['projects.language' => $language]);
            $reviews = Review::where(['language' => $language])->orderBy('date', 'DESC')->get();
            $highlight_review = Review::inRandomOrder()->where(['language' => $language, 'score' => 5])->limit(1)->first();
            $addons = Addon::where(['language' => $language])->get();
            $vacancies = Vacancy::get(['vacancies.language' => $language]);
            $themes = Theme::get();

            if ($page === null) {
                $languages = [];
                return response()->view('errors.404', compact('page', 'links', 'languages', 'website', 'references', 'projects', 'brands', 'reviews', 'highlight_review', 'faqs', 'partners', 'addons', 'vacancies'))->setStatusCode(404);
            } else {
                $languages = $this->getLanguages($page->page_id);
                return view($page->template, compact('page', 'links', 'languages', 'website', 'references', 'projects', 'brands', 'reviews', 'highlight_review', 'faqs', 'partners', 'addons', 'themes', 'vacancies'));
            }
        }

    }

    private function isActiveLanguage($language) {

        $language = Language::where(['code' => $language])->first();

        if ($language === null) {
            return false;
        } else {
            session()->put('lang', $language->code);
            return true;
        }

    }

    private function currentLanguage() {

        if (session()->get('lang')) {
            return session()->get('lang');
        } else {
            $lang = Language::where(['standard' => 1])->first()->code;
            session()->put('lang', $lang);
            return $lang;
        }

    }

    private function getLinks($language) {

        $navigations = Navigation::
            where('language', $language)
            ->join('pages', 'navigations.page_id', '=', 'pages.page_id')
            ->select('navigations.key', 'pages.title', 'pages.url', 'navigations.title as custom_title', 'navigations.target', 'navigations.url as custom_url')
            ->orderBy('order', 'ASC')
            ->get()
            ->groupBy('key');

        return $navigations;

    }

    private function getFaqs($language) {

        $faqs = Faq::
            where('language', $language)
            ->select('*')
            ->orderBy('order', 'ASC')
            ->get()
            ->groupBy('group_key');

        return $faqs;

    }

    private function getLanguages($page_id) {

        $languages = Page::where('page_id', $page_id)->select(['url', 'language'])->get();
        return $languages;

    }

}
