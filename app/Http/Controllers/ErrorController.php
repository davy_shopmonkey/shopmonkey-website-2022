<?php

namespace App\Http\Controllers;
use App\Models\Page;
use App\Models\Language;
use App\Models\Navigation;
use App\Models\Website;
use App\Models\Reference;
use App\Models\Project;
use App\Models\Tag;
use App\Models\Brand;
use App\Models\Review;
use App\Models\Faq;
use App\Models\Partner;

use Illuminate\Http\Request;

class ErrorController extends Controller
{

    public function index($language, $dir, $subdir, $subsubdir) {

        $page = null;

        if (!$this->isActiveLanguage($language)) {

            $subdirectory = $language;
            return redirect($this->currentLanguage(). '/' . $dir .'/' . $subdir . '/'. $subsubdir);

        } else {

            $links = $this->getLinks($language);
            $faqs = $this->getFaqs($language);

            $website = Website::first();
            $brands = Brand::inRandomOrder()->limit(10)->orderBy('order', 'ASC')->get();
            $partners = Partner::orderBy('order', 'ASC')->get();
            $references = Reference::get(['references.language' => $language]);
            $projects = Project::get(['projects.language' => $language]);
            $reviews = Review::where(['language' => $language])->orderBy('date', 'DESC')->get();
            $highlight_review = Review::inRandomOrder()->where(['language' => $language, 'score' => 5])->limit(1)->first();

            $languages = [];
            return response()->view('errors.404', compact('page', 'links', 'languages', 'website', 'references', 'projects', 'brands', 'reviews', 'highlight_review', 'faqs', 'partners'))->setStatusCode(404);
        }

    }

    private function isActiveLanguage($language) {

        $language = Language::where(['code' => $language])->first();

        if ($language === null) {
            return false;
        } else {
            session()->put('lang', $language->code);
            return true;
        }

    }

    private function currentLanguage() {

        if (session()->get('lang')) {
            return session()->get('lang');
        } else {
            $lang = Language::where(['standard' => 1])->first()->code;
            session()->put('lang', $lang);
            return $lang;
        }

    }

    private function getLinks($language) {

        $navigations = Navigation::
            where('language', $language)
            ->join('pages', 'navigations.page_id', '=', 'pages.page_id')
            ->select('navigations.key', 'pages.title', 'pages.url', 'navigations.title as custom_title', 'navigations.target', 'navigations.url as custom_url')
            ->orderBy('order', 'ASC')
            ->get()
            ->groupBy('key');

        return $navigations;

    }

    private function getFaqs($language) {

        $faqs = Faq::
            where('language', $language)
            ->select('*')
            ->orderBy('order', 'ASC')
            ->get()
            ->groupBy('group_key');

        return $faqs;

    }

    private function getLanguages($page_id) {

        $languages = Page::where('page_id', $page_id)->select(['url', 'language'])->get();
        return $languages;

    }

}
