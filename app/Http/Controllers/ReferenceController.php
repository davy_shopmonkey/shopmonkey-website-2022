<?php

namespace App\Http\Controllers;

use App\Models\Reference;
use App\Models\Language;
use App\Models\Navigation;
use App\Models\Website;
use App\Models\Page;
use App\Models\Project;
use App\Models\Brand;
use App\Models\Vacancy;

use Illuminate\Http\Request;

class ReferenceController extends Controller
{

    public function index($language, $type, $subdirectory){

        if (!$this->isActiveLanguage($language)) {

            $subdirectory = $language;
            return redirect($this->currentLanguage(). '/' . $subdirectory .'/');

        }

        if (in_array($type, ['references', 'referenties'])) {

            return $this->loadReference($language, $type, $subdirectory);

        } else if (in_array($type, ['portfolio'])) {

            return $this->loadPortfolioItem($language, $type, $subdirectory);

        } else if (in_array($type, ['vacancies', 'vacatures'])) {

            return $this->loadVacancy($language, $type, $subdirectory);

        } else {

            $website = Website::first();
            $links = $this->getLinks($language);
            return response()->view('errors.404', compact('website', 'links'))->setStatusCode(404);

        }

    }

    private function getLinks($language) {

        $navigations = Navigation::
            where('language', $language)
            ->join('pages', 'navigations.page_id', '=', 'pages.page_id')
            ->select('navigations.key', 'pages.title', 'pages.url', 'navigations.title as custom_title', 'navigations.target', 'navigations.url as custom_url')
            ->orderBy('order', 'ASC')
            ->get()
            ->groupBy('key');

        return $navigations;

    }

    private function getLanguagesReference($item_id) {

        $languages = Reference::where('item_id', $item_id)->select(['url', 'language'])->get();
        return $languages;

    }

    private function getLanguagesProject($item_id) {

        $languages = Project::where('item_id', $item_id)->select(['url', 'language'])->get();
        return $languages;

    }

    private function getLanguagesVacancy($item_id) {

        $languages = Vacancy::where('item_id', $item_id)->select(['url', 'language'])->get();
        return $languages;

    }

    private function loadReference($language, $type, $subdirectory) {

        $url = $type.'/'.$subdirectory;
        $reference = Reference::where(['url' => $url, 'language' => $language])->first();

        $website = Website::first();
        $links = $this->getLinks($language);

        if (!$reference) {
            return response()->view('errors.404', compact('website', 'links'))->setStatusCode(404);
        } else {

            $page = (object)[];
            $page->metatitle = $reference['title'];
            $page->metadescription = $reference['intro'];
            $page->metakeywords = '';
            $page->color_setting = 'light';


            $languages = $this->getLanguagesReference($reference->item_id);

            $brands = Brand::inRandomOrder()->limit(10)->orderBy('order', 'ASC')->get();
            $projects = Project::get();
            $related_project = Project::get(['projects.language' => $language, 'projects.item_id' => $reference['related_portfolio']]);
            $reference['related_project'] = isset($related_project[0]) ? $related_project[0] : false;

            return view('pages.reference', compact('page', 'links', 'languages', 'website', 'reference', 'brands', 'projects'));

        }

    }

    private function loadPortfolioItem($language, $type, $subdirectory) {

        $url = $type.'/'.$subdirectory;
        $project = Project::where(['url' => $url, 'language' => $language])->first();
        $links = $this->getLinks($language);
        $website = Website::first();

        if (!$project) {
            return response()->view('errors.404', compact('website', 'links'))->setStatusCode(404);
        } else {

            $page = (object)[];
            $page->metatitle = $project['title'];
            $page->metadescription = $project['description'];
            $page->metakeywords = '';
            $page->color_setting = 'dark';


            $languages = $this->getLanguagesProject($project->item_id);

            $brands = Brand::inRandomOrder()->limit(10)->orderBy('order', 'ASC')->get();
            $projects = Project::get(['projects.language' => $language]);

            $filtered_projects = [];
            foreach($projects as $project_item) {
                if ($project['id'] != $project_item['id']) {
                    $filtered_projects[] = $project_item;
                }
            }

            $projects = $filtered_projects;

            return view($project->template, compact('page', 'links', 'languages', 'website', 'project', 'projects'));

        }

    }

    private function loadVacancy($language, $type, $subdirectory) {

        $url = $type.'/'.$subdirectory;
        $vacancy = Vacancy::first(['vacancies.url' => $url, 'vacancies.language' => $language]);
        $links = $this->getLinks($language);
        $website = Website::first();

        if (!$vacancy) {
            return response()->view('errors.404', compact('website', 'links'))->setStatusCode(404);
        } else {

            $page = (object)[];
            $page->metatitle = $vacancy['title'];
            $page->metadescription = $vacancy['description'];
            $page->metakeywords = '';
            $page->color_setting = 'dark';


            $languages = $this->getLanguagesVacancy($vacancy->item_id);

            $brands = Brand::inRandomOrder()->limit(10)->orderBy('order', 'ASC')->get();
            $vacancies = Vacancy::get(['vacancies.language' => $language]);



            $filtered_vacancies = [];
            foreach($vacancies as $vacancy_item) {
                if ($vacancy['id'] != $vacancy_item['id']) {
                    $filtered_vacancies[] = $vacancy_item;
                }
            }

            $vacancies = $filtered_vacancies;

            return view('pages.vacancy', compact('page', 'links', 'languages', 'website', 'vacancy', 'vacancies'));

        }

    }

    private function currentLanguage() {

        if (session()->get('lang')) {
            return session()->get('lang');
        } else {
            $lang = Language::where(['standard' => 1])->first()->code;
            session()->put('lang', $lang);
            return $lang;
        }

    }

    private function isActiveLanguage($language) {

        $language = Language::where(['code' => $language])->first();

        if ($language === null) {
            return false;
        } else {
            session()->put('lang', $language->code);
            return true;
        }

    }

}
