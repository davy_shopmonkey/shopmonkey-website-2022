<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\GoogleService;
use App\Models\Ticket;

class ContactController extends Controller
{

    public function index(Request $request)
    {

     if(isset($_POST) && !empty($_POST['email'])){

         $googleService = new GoogleService;
         $verified = $googleService->verifyCaptcha($_POST["g-recaptcha-response"]);

         if(!$verified) {
            return response()->json([
              'status' => 'error',
              'message' => 'Robot!',
            ]);
         }

         $ticket = Ticket::create([
             'name' => $_POST['name'],
             'email' => $_POST['email'],
             'phone' => $_POST['phone'],
             'subject' => $_POST['subject'],
             'subject_other' => $_POST['subject_other'],
             'message' => $_POST['message'],
         ]);

         $message = "Beste Shopmonkey,<br><br>Er is een bericht binnengekomen via het contactformulier. Zie de gegevens hieronder:<br><br>".$_POST['name']."<br>".$_POST['email']."<br>".$_POST['phone']."<br><br><strong>Bericht:</strong> ".$_POST['message']."";

         $curl = curl_init();

         $postfields = [
             'name'         => $_POST['name'],
             'subject'      => '[Shopmonkey] Re: '. ($_POST['subject_other'] ? $_POST['subject_other'] : $_POST['subject']),
             'description'  => $message,
             'status'       => 2,
             'priority'     => 2,
             'tags'         => ['SITE', 'AR', strtoupper(session()->get('lang'))],
             'email'        => $_POST['email']
         ];

         curl_setopt_array($curl, array(
           CURLOPT_URL => 'https://onlinemonkey.freshdesk.com/api/v2/tickets',
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => '',
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 0,
           CURLOPT_FOLLOWLOCATION => true,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => 'POST',
           CURLOPT_POSTFIELDS => json_encode($postfields),
           CURLOPT_HTTPHEADER => array(
             'Content-Type: application/json',
             'Authorization: Basic UE9zTzNJTFppU3poUFV6SzkyTTpY',
             'Cookie: _x_w=32_1'
           ),
         ));

         $response = curl_exec($curl);
         $response = json_decode($response);
         curl_close($curl);

         $ticket->ticket_id = $response->id;
         $ticket->save();

        // $path = $request->file('filename')->store();

         return response()->json([
             'status' => 'success',
             'message' => translate('contact-success'),
             'path' => $request->all()
         ]);

     } else {

         return response()->json([
             'status' => 'error', 'message' => translate('contact-error')
         ]);

     }

    }

}
