<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Newsletter;
use App\Services\GoogleService;

class NewsletterController extends Controller
{

    public function index()
    {

     if(isset($_POST) && !empty($_POST['email'])){

         $googleService = new GoogleService;
         $verified = $googleService->verifyCaptcha($_POST["g-recaptcha-response"]);

         if (!$verified) {
            return response()->json([
                'status' => 'error',
                'message' => 'Robot!',
            ]);
         }

         Newsletter::subscribe($_POST['email']);
         Newsletter::addTags([strtoupper($_POST['lang'])], $_POST['email']);

         return response()->json([
             'status' => 'success',
             'message' => translate('newsletter-success'),
         ]);

     } else {

         return response()->json([
             'status' => 'error', 'message' => translate('newsletter-error')
         ]);

     }

    }

}
