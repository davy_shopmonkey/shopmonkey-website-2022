<?php

namespace App\Http\Controllers;
use App\Models\Page;
use App\Models\Reference;
use App\Models\Project;
use App\Models\Language;
use Spatie\ArrayToXml\ArrayToXml;

use Illuminate\Http\Request;

class SitemapController extends Controller
{

    public function index() {

        $languages = Language::get();

        $array = [];

        foreach($languages as $language) {
            $array['sitemap'][]['loc'] = url($language->code.'/sitemap.xml');
        }

        // <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd">
        // <sitemap>
        // <loc>https://www.malelions.com/nl/sitemap.xml</loc>
        // </sitemap>
        // <sitemap>
        // <loc>https://www.malelions.com/en/sitemap.xml</loc>
        // </sitemap>
        // </sitemapindex>

        // echo '<pre>';
        // print_r($array);
        // echo '</pre>';

        $result = ArrayToXml::convert($array, [
            'rootElementName' => 'sitemapindex',
            '_attributes' => [
                'xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9',
                'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
                'xsi:schemaLocation' => 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd'
            ],
        ]);

        return response($result, 200)->header('Content-Type', 'application/xml');

    }

    public function language($language) {

        header('Content-type: text/xml');

        $pages = Page::where(['language' => $language])->get();
        $references = Reference::where(['language' => $language])->get();
        $projects = Project::where(['language' => $language])->get();

        $array = [];
        foreach($pages as $page){
            $array['url'][] = [
                'loc' => url($page->language. '/' .$page->url),
                'lastmod' => date('Y-m-d', strtotime($page->updated_at)),
                'changefreq' => 'monthly',
                'priority' => '0.8'
            ];
        }

        foreach($references as $reference){
            $array['url'][] = [
                'loc' => url($reference->language. '/' .$reference->url),
                'lastmod' => date('Y-m-d', strtotime($reference->updated_at)),
                'changefreq' => 'monthly',
                'priority' => '0.8'
            ];
        }

        foreach($projects as $project){
            $array['url'][] = [
                'loc' => url($project->language. '/' .$project->url),
                'lastmod' => date('Y-m-d', strtotime($project->updated_at)),
                'changefreq' => 'monthly',
                'priority' => '0.8'
            ];
        }

        $result = ArrayToXml::convert($array, [
            'rootElementName' => 'urlset',
            '_attributes' => [
                'xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9',
                'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
                'xsi:schemaLocation' => 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd'
            ],
        ]);

        return response($result, 200)->header('Content-Type', 'application/xml');

    }

}
