<?php

    use App\Models\Translation;

    function translate($key, $replace = '', $replace2 = '') {

        $translation = Translation::where(['key' => $key])->first();
        $language = session()->get('lang');

        if ($translation === null) {
            $return = str_replace('%1', $replace, $key);
            $return = str_replace('%2', $replace2, $return);
            return $return;
        } else {
            $return = str_replace('%1', $replace, $translation->$language);
            $return = str_replace('%2', $replace2, $return);
            return $return;
        }

    }

    function smUrl($slug) {

        $language = session()->get('lang');
        return url($language. '/' .$slug);

    }

    function money($number, $decimals = 2, $decPoint = ',', $thousandsSep = '.'){

        $money = number_format($number, $decimals, $decPoint, $thousandsSep);
        $money = str_replace(',00', ',-', $money);
        return '€'.$money;

    }

