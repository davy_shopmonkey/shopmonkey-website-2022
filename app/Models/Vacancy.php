<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    use HasFactory;

    public static function get($where = [])
    {
        return self::select("vacancies.*",\DB::raw("GROUP_CONCAT(tags.title) as tags"))
        // ->orderBy('order', 'ASC')
        ->where($where)
        ->leftjoin("tags", function($leftJoin) {
            $leftJoin
            ->on(\DB::raw("FIND_IN_SET(tags.item_id, vacancies.tags)"),">",\DB::raw("'0'"))
            ->on('tags.language', '=', 'vacancies.language');
        })
        ->groupBy("vacancies.id")
        ->get();
    }

    public static function first($where = [])
    {
        return self::select("vacancies.*",\DB::raw("GROUP_CONCAT(tags.title) as tags"))
        // ->orderBy('order', 'ASC')
        ->where($where)
        ->leftjoin("tags", function($leftJoin) {
            $leftJoin
            ->on(\DB::raw("FIND_IN_SET(tags.item_id, vacancies.tags)"),">",\DB::raw("'0'"))
            ->on('tags.language', '=', 'vacancies.language');
        })
        ->groupBy("vacancies.id")
        ->first();
    }

}
