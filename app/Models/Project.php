<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    public static function get($where = [])
    {
        return self::select("projects.*",\DB::raw("GROUP_CONCAT(tags.title) as tags"))
        ->orderBy('order', 'ASC')
        ->where($where)
        ->leftjoin("tags", function($leftJoin) {
            $leftJoin
            ->on(\DB::raw("FIND_IN_SET(tags.item_id, projects.tags)"),">",\DB::raw("'0'"))
            ->on('tags.language', '=', 'projects.language');
        })
        ->groupBy("projects.id")
        ->get();
    }

}


