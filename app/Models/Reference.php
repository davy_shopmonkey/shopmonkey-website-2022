<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
    use HasFactory;

    public static function get($where = [])
    {
        return self::select("references.*",\DB::raw("GROUP_CONCAT(tags.title) as tags"))
        ->where($where)
        ->leftjoin("tags", function($leftJoin) {
            $leftJoin
            ->on(\DB::raw("FIND_IN_SET(tags.item_id, references.tags)"),">",\DB::raw("'0'"))
            ->on('tags.language', '=', 'references.language');
        })
        ->groupBy("references.id")
        ->get()->reverse();
    }

}

